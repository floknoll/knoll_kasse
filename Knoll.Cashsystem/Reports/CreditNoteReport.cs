﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class CreditNoteReport
    {
        public List<CeditNoteItem> CreditNote { get; set; }
    }

    public class CeditNoteItem
    {
        public Coupon Coupon { get; set; }

        public decimal Value
        {
            get
            {
                return Coupon.Value;
            }
        }

        public byte[] QRCodeArray { get; set; }
    }
}
