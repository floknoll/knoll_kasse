﻿namespace Knoll.Cashsystem
{
    partial class InventoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewArticle = new System.Windows.Forms.ListView();
            this.columnArticleNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnArticle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBarcode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnStock = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblArticleNo = new System.Windows.Forms.Label();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.lblArticle = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtArticleNo = new System.Windows.Forms.TextBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.btnInputHelp = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listViewArticle
            // 
            this.listViewArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewArticle.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnArticleNo,
            this.columnArticle,
            this.columnBarcode,
            this.columnStock});
            this.listViewArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.listViewArticle.Location = new System.Drawing.Point(12, 51);
            this.listViewArticle.Name = "listViewArticle";
            this.listViewArticle.Size = new System.Drawing.Size(500, 263);
            this.listViewArticle.TabIndex = 43;
            this.listViewArticle.UseCompatibleStateImageBehavior = false;
            this.listViewArticle.View = System.Windows.Forms.View.Details;
            this.listViewArticle.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewArticle_MouseDoubleClick);
            // 
            // columnArticleNo
            // 
            this.columnArticleNo.Text = "Artikel Nr.";
            this.columnArticleNo.Width = 72;
            // 
            // columnArticle
            // 
            this.columnArticle.Text = "Artikel";
            this.columnArticle.Width = 193;
            // 
            // columnBarcode
            // 
            this.columnBarcode.Text = "Barcode";
            this.columnBarcode.Width = 151;
            // 
            // columnStock
            // 
            this.columnStock.Text = "Lagerstand";
            this.columnStock.Width = 80;
            // 
            // lblArticleNo
            // 
            this.lblArticleNo.AutoSize = true;
            this.lblArticleNo.Location = new System.Drawing.Point(12, 4);
            this.lblArticleNo.Name = "lblArticleNo";
            this.lblArticleNo.Size = new System.Drawing.Size(53, 13);
            this.lblArticleNo.TabIndex = 44;
            this.lblArticleNo.Text = "Artikel Nr.";
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(361, 4);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(47, 13);
            this.lblBarcode.TabIndex = 45;
            this.lblBarcode.Text = "Barcode";
            // 
            // lblArticle
            // 
            this.lblArticle.AutoSize = true;
            this.lblArticle.Location = new System.Drawing.Point(184, 4);
            this.lblArticle.Name = "lblArticle";
            this.lblArticle.Size = new System.Drawing.Size(69, 13);
            this.lblArticle.TabIndex = 46;
            this.lblArticle.Text = "Bezeichnung";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(187, 20);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(148, 20);
            this.txtDescription.TabIndex = 2;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // txtArticleNo
            // 
            this.txtArticleNo.Location = new System.Drawing.Point(12, 20);
            this.txtArticleNo.Name = "txtArticleNo";
            this.txtArticleNo.Size = new System.Drawing.Size(148, 20);
            this.txtArticleNo.TabIndex = 1;
            this.txtArticleNo.TextChanged += new System.EventHandler(this.txtArticleNo_TextChanged);
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(364, 20);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(148, 20);
            this.txtBarcode.TabIndex = 3;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            // 
            // btnInputHelp
            // 
            this.btnInputHelp.Location = new System.Drawing.Point(264, 320);
            this.btnInputHelp.Name = "btnInputHelp";
            this.btnInputHelp.Size = new System.Drawing.Size(121, 23);
            this.btnInputHelp.TabIndex = 4;
            this.btnInputHelp.Text = "Eingabehilfe starten";
            this.btnInputHelp.UseVisualStyleBackColor = true;
            this.btnInputHelp.Click += new System.EventHandler(this.btnInputHelp_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(391, 320);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Schließen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // InventoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(525, 348);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnInputHelp);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.txtArticleNo);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblArticle);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.lblArticleNo);
            this.Controls.Add(this.listViewArticle);
            this.Name = "InventoryForm";
            this.Text = "Inventory";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewArticle;
        private System.Windows.Forms.ColumnHeader columnArticleNo;
        private System.Windows.Forms.ColumnHeader columnArticle;
        private System.Windows.Forms.ColumnHeader columnBarcode;
        private System.Windows.Forms.ColumnHeader columnStock;
        private System.Windows.Forms.Label lblArticleNo;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Label lblArticle;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtArticleNo;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Button btnInputHelp;
        private System.Windows.Forms.Button btnCancel;
    }
}