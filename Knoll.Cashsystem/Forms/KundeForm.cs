﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class Kunde : Form
    {

        public Kunde()
        {
            InitializeComponent();
        }

        public void FillCustomerList(List<Product> products)
        {
            this.listViewArtikelKunde.Items.Clear();
            foreach (var p in products)
            {
                ListViewItem item = null;
                if (p.IsCoupon)
                {
                    p.Description = "Gutschein:   " + string.Format("{0:n} €", p.Price);
                }
                item = new ListViewItem(p.Description);
                if (p.Amount != 0)
                {
                    item.SubItems.Add(p.Amount.ToString());
                }
                else
                {
                    item.SubItems.Add("");
                }
                item.SubItems.Add(string.Format("{0:n} €", p.Price));
                item.SubItems.Add(string.Format("{0:n} €", p.TotalPrice));
                item.Tag = p;
                this.listViewArtikelKunde.Items.Add(item);
            }
            decimal total = products.Sum(s => s.TotalPrice);
            lblTotalPrice.Text = string.Format(("€   {0:n}"), total);
        }        

        public void DoPayInCustomerList(decimal total, decimal incomeMoney, decimal cashBack)
        {
            ListViewItem item = null;
            item = new ListViewItem("---------------------------------------------------------------");
            item.SubItems.Add("------------");
            item.SubItems.Add("---------------");
            item.SubItems.Add("---------------");
            this.listViewArtikelKunde.Items.Add(item);
            item = null;
            item = new ListViewItem("Total:");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add(string.Format("€ {0:n}", total));
            this.listViewArtikelKunde.Items.Add(item);
            item = null;
            item = new ListViewItem("---------------------------------------------------------------");
            item.SubItems.Add("------------");
            item.SubItems.Add("---------------");
            item.SubItems.Add("---------------");
            this.listViewArtikelKunde.Items.Add(item);
            item = null;
            item = new ListViewItem("Bar:");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add(string.Format("€ {0:n}", incomeMoney));
            this.listViewArtikelKunde.Items.Add(item);
            item = null;
            item = new ListViewItem("Rückgeld Bar:");
            item.SubItems.Add("");
            item.SubItems.Add("");
            item.SubItems.Add(string.Format("€ {0:n}", cashBack));
            this.listViewArtikelKunde.Items.Add(item);
        }

        public void Clear()
        {
            listViewArtikelKunde.Items.Clear();
            lblTotalPrice.Text = string.Empty;
        }
    }
}

