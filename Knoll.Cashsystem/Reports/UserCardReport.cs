﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class UserCardReport
    {
        public List<UserCardItem> Persons { get; set; }
    }

    public class UserCardItem
    {
        public Person Person { get; set; }

        public string FirstName
        {
            get
            {
                return Person.FirstName;
            }
        }

        public string LastName
        {
            get
            {
                return Person.LastName;
            }
        }


        public byte[] QRCodeArray { get; set; }
    }
}
