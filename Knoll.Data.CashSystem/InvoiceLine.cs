﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
    public class InvoiceLine : KnollDataObject
    {

        const string selectCmd = "select INVOICE_LINE_ID, INVOICE_ID, ARTICLE_ID, coupon_id, AMOUNT, PRICE, pos, description, tax from knoll_kasse.Invoice_Line";
        const string insertCmd = "insert into knoll_kasse.Invoice_Line (invoice_line_id, INVOICE_ID, ARTICLE_ID, coupon_id, AMOUNT, PRICE, pos, description, tax) values (:invoice_line_id, :invoice_id, :article_id, :coupon_id, :amount, :price, :pos, :description, :tax)";
        const string updateCmd = "update knoll_kasse.Invoice_Line set invoice_id = :invoice_id, article_id = :article_id, coupon_id = :coupon_id, amount = :amount, price = :price, pos = :pos, description = :description, tax = tax where invoice_line_id = :invoice_line_id";
        const string deleteCmd = "delete from knoll_kasse.Invoice_Line where invoice_line_id = :invoice_line_id";
        //***************************************************************
        #region static methods

        public static List<InvoiceLine> GetList() => GetList<InvoiceLine>($"{selectCmd} order by invoice_id");

        public static List<InvoiceLine> GetList(Invoice invoice) => GetList<InvoiceLine>(invoice, $"{selectCmd} where invoice_id = :invoice_id order by pos", new KeyValuePair<string, object>("invoice_id", invoice.InvoiceId));
        
        #endregion

        //***************************************************************
        #region constructors
        public InvoiceLine() : base()
        {
            SetValue("invoice_line_id", -1m);
        }

        public InvoiceLine(KnollDataObject parent) : base(parent)
        {
            SetValue("invoice_line_id", -1m);
        }

        public InvoiceLine(Dictionary<string, object> row) : base(row)
        {

        }

        public InvoiceLine(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
        {

        }
        #endregion

        //***************************************************************
        #region properties
        public decimal InvoiceLineId
        {
            get
            {
                return GetValue<decimal>("invoice_line_id");
            }
        }

        public decimal InvoiceId
        {
            get
            {
                return GetValue<decimal>("invoice_id");
            }
            set
            {
                SetValue("invoice_id", value);
            }
        }

        public decimal? ArticleId
        {
            get
            {
                return GetValue<decimal>("article_id");
            }
            set
            {
                SetValue("article_id", value);
            }
        }

        public decimal? CouponId
        {
            get
            {
                return GetValue<decimal>("coupon_id");
            }
            set
            {
                SetValue("coupon_id", value);
            }
        }

        public decimal Amount
        {
            get
            {
                return GetValue<decimal>("amount");
            }
            set
            {
                SetValue("amount", value);
            }
        }

        public decimal Price
        {
            get
            {
                return GetValue<decimal>("price");
            }
            set
            {
                SetValue("price", value);
            }
        }

        public decimal Position
        {
            get
            {
                return GetValue<decimal>("pos");
            }
            set
            {
                SetValue("pos", value);
            }
        }

        public string Description
        {
            get
            {
                return GetValue<string>("description");
            }
            set
            {
                SetValue("description", value);
            }
        }

        public string Tax
        {
            get
            {
                return GetValue<string>("tax");
            }
            set
            {
                SetValue("tax", value);
            }
        }

        #endregion

        //***************************************************************
        #region public methods

        public bool Save(NpgsqlTransaction transaction = null)
        {
            if (transaction == null)
                KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.Transaction = transaction;
            if (this.InvoiceLineId == -1)
            {
                command.CommandText = "select nextval('knoll_kasse.invoice_line_seq')";
                SetValue("invoice_line_id", (decimal)((long)command.ExecuteScalar()));
                command.CommandText = insertCmd;
            }
            else
            {
                command.CommandText = updateCmd;
            }
            command.Parameters.AddWithValue("invoice_line_id", this.InvoiceLineId);
            command.Parameters.AddWithValue("invoice_id", ((Invoice)this.parent).InvoiceId);
            if (CouponId != 0)
            {
                command.Parameters.AddWithValue("article_id", (object)DBNull.Value);
                command.Parameters.AddWithValue("coupon_id", this.CouponId);
            }
            else
            {
                command.Parameters.AddWithValue("coupon_id", (object)DBNull.Value);
                command.Parameters.AddWithValue("article_id", this.ArticleId);                
            }
            command.Parameters.AddWithValue("amount", this.Amount);
            command.Parameters.AddWithValue("price", this.Price);
            command.Parameters.AddWithValue("pos", this.Position);
            command.Parameters.AddWithValue("description", this.Description);
            command.Parameters.AddWithValue("tax", String.IsNullOrEmpty(this.Tax) ? (object)DBNull.Value : (object)this.Tax);

            int result = command.ExecuteNonQuery();

            if (transaction == null)
                KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }        

        public bool Delete()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = deleteCmd;
            command.Parameters.AddWithValue("invoice_line_id", this.InvoiceLineId);
            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;

        }

        #endregion


    }
}
