﻿using Knoll.BarcodeListener;
using Knoll.Cashsystem.Classes;
using Knoll.Cashsystem.Properties;
using Knoll.Cashsystem.Reports;
using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class KassenForm : Form
    {
        //Instanzierungen
        Kunde kunde = new Kunde();
        public List<Product> products = new List<Product>();
        BcListener listener = BcListener.GetBarcodeListener();

        private decimal cashBack = 0;

        LoginForm loginForm;
        public KassenForm(LoginForm loginForm)
        {
            InitializeComponent();
            this.loginForm = loginForm;
            // Barcode scan
            listener.OnProduktBarcodeDataReceived += Listener_OnProduktBarcodeDataReceived;
            
            lblOutput.Text = string.Empty;
        }

        private void Listener_OnProduktBarcodeDataReceived(object sender, EventArgs e)
        {
            Invoke(new Action(() => {
                var barcode = (e as ProduktEventArgs).Barcode.Split(':');
                if (barcode[0] == "GUT")
                {
                    if (products.Where(w => w.IsCoupon).Count() == 5)
                    {
                        MessageBox.Show("Pro Einkauf dürfen nur maximal 5 Gutscheine verwendet werden!");
                    }
                    else
                    {
                        Coupon coupon = Coupon.Get(barcode[1]);
                        if (coupon != null && coupon.Valid)
                        {
                            coupon.Valid = false;
                            coupon.Redeemed = DateTime.Now;
                            AddCouponToList(coupon);
                            coupon.Save();
                        }
                        else if (coupon != null && !coupon.Valid)
                        {
                            MessageBox.Show("Gutschein wurde bereits eingelöst!");
                        }
                        else if (coupon == null)
                        {
                            coupon = new Coupon();
                            coupon.SerialNo = barcode[1];
                            coupon.Spender = barcode[2];
                            coupon.Value = decimal.Parse(barcode[3]);
                            coupon.Valid = false;
                            coupon.Redeemed = DateTime.Now;
                            AddCouponToList(coupon);
                            coupon.Save();
                        }
                        else
                        {
                            MessageBox.Show("Falscher Barcode oder Gutschein wurde nicht erkannt! Bitte versuchen Sie es erneut.");
                        }
                    }
                }
                else if (barcode[0] != null)
                {
                    Article article = Article.Get(barcode[0]);
                    if (article != null)
                    {
                        AddArticleToList(article, 1);
                    }
                    else
                    {
                        MessageBox.Show("Falscher Barcode oder Artikel wurde nicht erkannt! Bitte versuchen Sie es erneut.");
                    }
                }
                
            }));
        }

        private void Kasse_Load(object sender, EventArgs e)
        {
            kunde.Show();
            this.DesktopLocation = new Point(-5, 10);
            kunde.DesktopLocation = new Point(860, 10);
        }

        #region Buttons

        private void btn1_Click(object sender, EventArgs e)
        {
            AddNumber("1");
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            AddNumber("2");
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            AddNumber("3");
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            AddNumber("4");
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            AddNumber("5");
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            AddNumber("6");
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            AddNumber("7");
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            AddNumber("8");
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            AddNumber("9");
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            AddNumber("0");
        }

        private void btn00_Click(object sender, EventArgs e)
        {
            AddNumber("00");
        }

        private void btnExpiry_Click(object sender, EventArgs e)
        {
            if (products.Count > 0)
            {
                Product product = products.Last();
                if (!product.IsCoupon && product.Price != 0)
                {
                    Article article = Article.Get(product.Barcode);
                    products.RemoveAt(products.Count - 1);
                    product.Price = article.Price;
                    product.TotalPrice = article.Price;
                    product.Description = article.Description;
                    product.Barcode = article.Barcode;
                    products.Add(product);

                    Product prod = new Product();
                    prod.Price = Math.Round((article.Price / 2), MidpointRounding.AwayFromZero) * -1;
                    prod.TotalPrice = prod.Price;
                    prod.Description = "   -50%";
                    prod.DiscountGranted = 3;
                    prod.Barcode = article.Barcode;
                    products.Add(prod);
                    FillArticleList();
                }
                else
                {
                    MessageBox.Show("Gutscheine können nicht reduziert werden!");
                }
            }
            else
            {
                MessageBox.Show("Kein Produkt eingegeben!");
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lblOutput.Text.Length != 0)
            {
                lblOutput.Text = "";
            }              
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (lblOutput.Text.Length > 6)
            {
                int amount = 1;
                Article article = Article.Get(lblOutput.Text);              
                if (article != null)
                {
                    AddArticleToList(article, amount);
                }
                else
                {
                    MessageBox.Show("Kein Artikel mit diesem Barcode gefunden!");
                }
            }
            else
            {
                MessageBox.Show("Ungültiger Barcode!");
            }
            lblOutput.Text = "";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            listViewArtikel.Items.Clear();
            lblTotalPrice.Text = string.Empty;
            lblOutput.Text = string.Empty;
            kunde.Clear();
            products = new List<Product>();
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lblOutput.Text))
            {
                if (lblOutput.Text.Length <= 6)
                {
                    decimal total = products.Sum(s => s.TotalPrice);
                    decimal incomeMoney = decimal.Parse(lblOutput.Text) / 100;
                    cashBack = incomeMoney - total;
                    ListViewItem item = null;
                    item = new ListViewItem("----------------------------------------------------------");
                    item.SubItems.Add("------------");
                    item.SubItems.Add("---------------");
                    item.SubItems.Add("-------------------");
                    this.listViewArtikel.Items.Add(item);
                    item = null;
                    item = new ListViewItem("Total:");
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                    item.SubItems.Add(string.Format("€ {0:n}", total));
                    this.listViewArtikel.Items.Add(item);
                    item = null;
                    item = new ListViewItem("----------------------------------------------------------");
                    item.SubItems.Add("------------");
                    item.SubItems.Add("---------------");
                    item.SubItems.Add("-------------------");
                    this.listViewArtikel.Items.Add(item);
                    item = null;
                    item = new ListViewItem("Bar:");
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                    item.SubItems.Add(string.Format("€ {0:n}", incomeMoney));
                    this.listViewArtikel.Items.Add(item);
                    item = null;
                    item = new ListViewItem("Rückgeld Bar:");
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                    item.SubItems.Add(string.Format("€ {0:n}", Math.Round(cashBack, 2)));
                    this.listViewArtikel.Items.Add(item);
                    kunde.DoPayInCustomerList(total, incomeMoney, Math.Round(cashBack, 2));
                    if (cashBack != 0)
                    {
                        printDialogCashBack.ShowDialog();
                        printDocumentCashBack.PrinterSettings = printDialogCashBack.PrinterSettings;
                        printDocumentCashBack.PrintPage += printDocumentCashBack_PrintPage;
                        printDocumentCashBack.DocumentName = "Rückgeld";
                        printDocumentCashBack.Print();
                    }
                    CreateInvoice(cashBack);
                }
                else
                {
                    MessageBox.Show("Ungültige Eingabe!");
                }
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Functions

        private void AddNumber(string number)
        {
                lblOutput.Text = lblOutput.Text + number;
        }

        public void AddArticleToList(Article article, int amount)
        {
            decimal discountGranted = 0;
            decimal discountArticleGranted = 0;
            Article discountArticle = null;
            string description = article.Description;
            decimal price = article.Price;
            if (article.DiscountId == 0 || article.DiscountId == 1)
            {
                if (article.ArticleCategoryDiscount != null)
                {
                    if (article.ArticleCategoryDiscount.DiscountId != 0 && article.ArticleCategoryDiscount.DiscountId != 1)
                    {
                        if (article.ArticleCategoryDiscount.OnePlusOne)
                        {
                            if (products.Where(w => w.ArticleNo == article.ArticleNo).Count() % 2 == 1)
                            {
                                price = 0;
                                description = article.Description + "  1 + 1 Gratis";
                                discountGranted = article.ArticleCategoryDiscount.DiscountId;
                            }
                        }
                        else if (!article.ArticleCategoryDiscount.OnePlusOne && article.ArticleCategoryDiscount.Percent.HasValue && article.ArticleCategoryDiscount.Percent != 0)
                        {
                            discountArticle = new Article();
                            decimal percent = article.ArticleCategoryDiscount.Percent.Value;
                            discountArticle.Description = string.Format("- {0}%", article.ArticleCategoryDiscount.Percent);
                            discountArticle.Price = ((article.Price / 100) * percent) * -1;
                            discountGranted = article.ArticleCategoryDiscount.DiscountId;
                        }
                    }
                }
            }
            else
            {
                if (article.ArticleDiscount != null)
                {
                    if (article.ArticleDiscount.DiscountId != 1)
                    {
                        if (article.ArticleDiscount.OnePlusOne)
                        {
                            var cnt = products.Where(w => w.ArticleNo == article.ArticleNo).Count();
                            if (products.Where(w => w.ArticleNo == article.ArticleNo).Count() % 2 == 1)
                            {
                                price = 0;
                                description = article.Description + "  1 + 1 Gratis";
                                discountGranted = article.ArticleDiscount.DiscountId;
                            }
                        }
                        else if (!article.ArticleDiscount.OnePlusOne && article.ArticleDiscount.Percent.HasValue)
                        {
                            discountArticle = new Article();
                            decimal percent = article.ArticleDiscount.Percent.Value;
                            discountArticle.Description = string.Format("  - {0}%", article.ArticleDiscount.Percent);
                            discountArticle.Price = ((article.Price / 100) * percent) * -1;
                            discountGranted = article.ArticleDiscount.DiscountId;
                        }
                    }
                }
            }
            Product product = new Product
            {
                ArticleNo = article.ArticleNo,
                Barcode = article.Barcode,
                Description = description,
                Amount = amount,
                Price = price,
                TotalPrice = amount * price,
                DiscountGranted = discountGranted,
                IsCoupon = false
            };
            this.products.Add(product);  
            if (discountArticle != null)
            {
                product = new Product
                {
                    ArticleNo = article.ArticleNo,
                    Barcode = article.Barcode,
                    Description = discountArticle.Description,
                    Amount = 0,
                    Price = discountArticle.Price,
                    TotalPrice = discountArticle.Price,
                    DiscountGranted = 0,
                    IsCoupon = false
                };
                this.products.Add(product);
            }
            FillArticleList();
          
        }

        private void AddCouponToList(Coupon coupon)
        {
            Product product = new Product
            {
                ArticleNo = null,
                Barcode = coupon.SerialNo,
                Description = "Gutschein:   " + coupon.Value.ToString(),
                Amount = 1,
                Price = coupon.Value * -1,
                TotalPrice = coupon.Value * -1,
                IsCoupon = true
            };
            this.products.Add(product);
            FillArticleList();
            kunde.FillCustomerList(products);
        }

        private void FillArticleList()
        {
            this.listViewArtikel.Items.Clear();
            foreach (var p in products)
            {
                ListViewItem item = null;
                if (p.IsCoupon)
                {
                    p.Description = "Gutschein:   " + string.Format("€   {0:n}", p.Price);
                }                
                item = new ListViewItem(p.Description);
                if (p.Amount != 0)
                {
                    item.SubItems.Add(p.Amount.ToString());
                }
                else
                {
                    item.SubItems.Add("");
                }
                item.SubItems.Add(string.Format("{0:n} €", p.Price));
                item.SubItems.Add(string.Format("{0:n} €", p.TotalPrice));
                item.Tag = p;
                this.listViewArtikel.Items.Add(item);                
                kunde.FillCustomerList(products);
            }
            decimal total = products.Sum(s => s.TotalPrice);
            this.lblTotalPrice.Text = string.Format("€   {0:n}", total);
        }

        private void CreateInvoice(decimal backMoney)
        {
            Invoice invoice = new Invoice();
            var total = products.Sum(s => s.TotalPrice);
            invoice.Brutto = total;
            invoice.InvoiceDate = DateTime.Now;
            invoice.BackMoney = Math.Round(backMoney, 2);
            invoice.Save();
            int pos = 1;
            decimal nettoA = 0;
            decimal nettoB = 0;
            foreach (var product in products)
            {
                if (!product.IsCoupon)
                {
                    Article article = Article.Get(product.Barcode);
                    article.Stock -= 1;
                    article.Save();
                    InvoiceLine invoiceLine = invoice.InvoiceLineAdd(article.ArticleId, product.Amount, product.TotalPrice, pos, product.Description, article.Category.Tax, false);
                    if (product.DiscountGranted != 1 && product.DiscountGranted != 0)
                    {
                        DiscountGranted discountGranted = new DiscountGranted();
                        discountGranted.DiscountId = product.DiscountGranted;
                        discountGranted.ArticleId = article.ArticleId;
                        discountGranted.InvoiceId = invoice.InvoiceId;
                        discountGranted.Save();
                    }
                    if (article.Category.Tax == "A")
                    {
                        nettoA += product.Price;
                    }
                    else if (article.Category.Tax == "B")
                    {
                        nettoB += product.Price;
                    }
                }
                else
                {
                    Coupon coupon = Coupon.Get(product.Barcode);
                    InvoiceLine invoiceLine = invoice.InvoiceLineAdd(coupon.CouponId, product.Amount, product.TotalPrice, pos, product.Description, null, true);
                }
                pos++;
                
            }
            invoice.NettoA = nettoA;
            invoice.NettoB = nettoB;
            invoice.Save();
            byte[] qrCode = GenerateBarcode(invoice);
            BillReport billReport = new BillReport();
            billReport.PrintBill(invoice.InvoiceId, qrCode);
        }

        private byte[] GenerateBarcode(Invoice invoice)
        {
            var settings = new Spire.Barcode.BarcodeSettings();
            settings.Type = Spire.Barcode.BarCodeType.QRCode;
            settings.Data = Crypt.HashSHAPassword(invoice.InvoiceNo.ToString()) + invoice.Brutto;
            Spire.Barcode.BarCodeGenerator generator = new Spire.Barcode.BarCodeGenerator(settings);
            Image qrCode = generator.GenerateImage();            
            Bitmap bmp = new Bitmap(qrCode);
            Graphics g = Graphics.FromImage(bmp);
            Image empty = Resources.empty;
            g.DrawImage(empty, 0, 0);
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }

        public List<Image> GetChangeMoneyImages(decimal rueckGeld)
        {
            List<Image> changeMoney = new List<Image>();            
            decimal[] euro_cent = new decimal[] { 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.50m, 0.20m, 0.10m, 0.05m, 0.02m, 0.01m };
            rueckGeld = Math.Round(rueckGeld, 2);
            while (rueckGeld > 0)
            {
                foreach (var item in euro_cent)
                {
                    if (item <= rueckGeld)
                    {
                        rueckGeld -= item;
                        switch (item)
                        {
                            case 500:
                                changeMoney.Add(Resources._500euro);
                                break;
                            case 200:
                                changeMoney.Add(Resources._200euro);
                                break;
                            case 100:
                                changeMoney.Add(Resources._100euro);
                                break;
                            case 50:
                                changeMoney.Add(Resources._50euro);
                                break;
                            case 20:
                                changeMoney.Add(Resources._20euro);
                                break;
                            case 10:
                                changeMoney.Add(Resources._10euro);
                                break;
                            case 5:
                                changeMoney.Add(Resources._5euro);
                                break;
                            case 2:
                                changeMoney.Add(Resources._2euro);
                                break;
                            case 1:
                                changeMoney.Add(Resources._1euro);
                                break;
                            case 0.50m:
                                changeMoney.Add(Resources._50cent);
                                break;
                            case 0.20m:
                                changeMoney.Add(Resources._20cent);
                                break;
                            case 0.10m:
                                changeMoney.Add(Resources._10cent);
                                break;
                            case 0.05m:
                                changeMoney.Add(Resources._5cent);
                                break;
                            case 0.02m:
                                changeMoney.Add(Resources._2cent);
                                break;
                            case 0.01m:
                                changeMoney.Add(Resources._1cent);
                                break;
                        }
                        break;
                    }
                }
            }
            return changeMoney;
        }

        private void printDocumentCashBack_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            var g = e.Graphics;
            List<Image> changeMoney = GetChangeMoneyImages(cashBack);
            int posX = 20;
            int posY = 5;
            foreach (var image in changeMoney)
            {
                g.DrawImage(image, posY, posX);
                posX += 200;
                if (posX > 1400)
                {
                    posX = 20;
                    posY = 300;
                }
            }
        }

        #endregion

        #region Properties
        
        public static string Total { get; set; }

        #endregion

        private void btnInventory_Click(object sender, EventArgs e)
        {
            InventoryForm inventoryForm = new InventoryForm();
            inventoryForm.Show();
        }

        private void KassenForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            listener.OnProduktBarcodeDataReceived -= Listener_OnProduktBarcodeDataReceived;
            kunde.Close();
            loginForm.HidePasswordChange();
            loginForm.HideNewPasswordChange();
            loginForm.SetVisible();
        }
    }
}
