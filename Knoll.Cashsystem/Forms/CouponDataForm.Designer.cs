﻿namespace Knoll.Cashsystem
{
    partial class CouponDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblSerialNo = new System.Windows.Forms.Label();
            this.txtSerialNo = new System.Windows.Forms.TextBox();
            this.lblSpender = new System.Windows.Forms.Label();
            this.txtSpender = new System.Windows.Forms.TextBox();
            this.chkValid = new System.Windows.Forms.CheckBox();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.lblValue = new System.Windows.Forms.Label();
            this.txtRedeemed = new System.Windows.Forms.TextBox();
            this.lblRedeemed = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(197, 223);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Abbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(116, 223);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 9;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblSerialNo
            // 
            this.lblSerialNo.AutoSize = true;
            this.lblSerialNo.Location = new System.Drawing.Point(9, 9);
            this.lblSerialNo.Name = "lblSerialNo";
            this.lblSerialNo.Size = new System.Drawing.Size(74, 13);
            this.lblSerialNo.TabIndex = 11;
            this.lblSerialNo.Text = "Seriennummer";
            // 
            // txtSerialNo
            // 
            this.txtSerialNo.Location = new System.Drawing.Point(12, 28);
            this.txtSerialNo.Name = "txtSerialNo";
            this.txtSerialNo.Size = new System.Drawing.Size(260, 20);
            this.txtSerialNo.TabIndex = 12;
            // 
            // lblSpender
            // 
            this.lblSpender.AutoSize = true;
            this.lblSpender.Location = new System.Drawing.Point(9, 62);
            this.lblSpender.Name = "lblSpender";
            this.lblSpender.Size = new System.Drawing.Size(52, 13);
            this.lblSpender.TabIndex = 14;
            this.lblSpender.Text = "Ausgeber";
            // 
            // txtSpender
            // 
            this.txtSpender.Location = new System.Drawing.Point(12, 78);
            this.txtSpender.Name = "txtSpender";
            this.txtSpender.Size = new System.Drawing.Size(260, 20);
            this.txtSpender.TabIndex = 15;
            // 
            // chkValid
            // 
            this.chkValid.AutoSize = true;
            this.chkValid.Location = new System.Drawing.Point(219, 183);
            this.chkValid.Name = "chkValid";
            this.chkValid.Size = new System.Drawing.Size(53, 17);
            this.chkValid.TabIndex = 16;
            this.chkValid.Text = "Gültig";
            this.chkValid.UseVisualStyleBackColor = true;
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(12, 127);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(132, 20);
            this.txtValue.TabIndex = 18;
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Location = new System.Drawing.Point(9, 111);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(30, 13);
            this.lblValue.TabIndex = 17;
            this.lblValue.Text = "Wert";
            // 
            // txtRedeemed
            // 
            this.txtRedeemed.Location = new System.Drawing.Point(12, 180);
            this.txtRedeemed.Name = "txtRedeemed";
            this.txtRedeemed.Size = new System.Drawing.Size(132, 20);
            this.txtRedeemed.TabIndex = 20;
            // 
            // lblRedeemed
            // 
            this.lblRedeemed.AutoSize = true;
            this.lblRedeemed.Location = new System.Drawing.Point(9, 164);
            this.lblRedeemed.Name = "lblRedeemed";
            this.lblRedeemed.Size = new System.Drawing.Size(50, 13);
            this.lblRedeemed.TabIndex = 19;
            this.lblRedeemed.Text = "Eingelöst";
            // 
            // CouponDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(284, 253);
            this.Controls.Add(this.txtRedeemed);
            this.Controls.Add(this.lblRedeemed);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.chkValid);
            this.Controls.Add(this.txtSpender);
            this.Controls.Add(this.lblSpender);
            this.Controls.Add(this.txtSerialNo);
            this.Controls.Add(this.lblSerialNo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "CouponDataForm";
            this.Text = "Gutschein bearbeiten";
            this.Load += new System.EventHandler(this.CouponDataForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblSerialNo;
        private System.Windows.Forms.TextBox txtSerialNo;
        private System.Windows.Forms.Label lblSpender;
        private System.Windows.Forms.TextBox txtSpender;
        private System.Windows.Forms.CheckBox chkValid;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Label lblValue;
        private System.Windows.Forms.TextBox txtRedeemed;
        private System.Windows.Forms.Label lblRedeemed;
    }
}