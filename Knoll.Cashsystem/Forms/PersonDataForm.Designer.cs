﻿namespace Knoll.Cashsystem
{
    partial class PersonDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPersonalNo = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblNickName = new System.Windows.Forms.Label();
            this.lblPersonType = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtNickName = new System.Windows.Forms.TextBox();
            this.txtPersonalNo = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.cmbPersonType = new System.Windows.Forms.ComboBox();
            this.btnResetPW = new System.Windows.Forms.Button();
            this.txtPasswordExp = new System.Windows.Forms.Label();
            this.txtLoginAtt = new System.Windows.Forms.Label();
            this.txtAccountStatus = new System.Windows.Forms.Label();
            this.btnUnlockUser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(193, 275);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(274, 275);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Abbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblPersonalNo
            // 
            this.lblPersonalNo.AutoSize = true;
            this.lblPersonalNo.Location = new System.Drawing.Point(12, 9);
            this.lblPersonalNo.Name = "lblPersonalNo";
            this.lblPersonalNo.Size = new System.Drawing.Size(65, 13);
            this.lblPersonalNo.TabIndex = 2;
            this.lblPersonalNo.Text = "Personal Nr.";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(12, 57);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(49, 13);
            this.lblFirstName.TabIndex = 3;
            this.lblFirstName.Text = "Vorname";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(194, 57);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(59, 13);
            this.lblLastName.TabIndex = 4;
            this.lblLastName.Text = "Nachname";
            // 
            // lblNickName
            // 
            this.lblNickName.AutoSize = true;
            this.lblNickName.Location = new System.Drawing.Point(12, 106);
            this.lblNickName.Name = "lblNickName";
            this.lblNickName.Size = new System.Drawing.Size(75, 13);
            this.lblNickName.TabIndex = 5;
            this.lblNickName.Text = "Benutzername";
            // 
            // lblPersonType
            // 
            this.lblPersonType.AutoSize = true;
            this.lblPersonType.Location = new System.Drawing.Point(12, 154);
            this.lblPersonType.Name = "lblPersonType";
            this.lblPersonType.Size = new System.Drawing.Size(31, 13);
            this.lblPersonType.TabIndex = 6;
            this.lblPersonType.Text = "Rolle";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(15, 73);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(152, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // txtNickName
            // 
            this.txtNickName.Location = new System.Drawing.Point(15, 122);
            this.txtNickName.Name = "txtNickName";
            this.txtNickName.Size = new System.Drawing.Size(152, 20);
            this.txtNickName.TabIndex = 3;
            // 
            // txtPersonalNo
            // 
            this.txtPersonalNo.Enabled = false;
            this.txtPersonalNo.Location = new System.Drawing.Point(15, 25);
            this.txtPersonalNo.Name = "txtPersonalNo";
            this.txtPersonalNo.Size = new System.Drawing.Size(100, 20);
            this.txtPersonalNo.TabIndex = 0;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(197, 73);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(152, 20);
            this.txtLastName.TabIndex = 2;
            // 
            // cmbPersonType
            // 
            this.cmbPersonType.FormattingEnabled = true;
            this.cmbPersonType.Location = new System.Drawing.Point(15, 170);
            this.cmbPersonType.Name = "cmbPersonType";
            this.cmbPersonType.Size = new System.Drawing.Size(152, 21);
            this.cmbPersonType.TabIndex = 5;
            // 
            // btnResetPW
            // 
            this.btnResetPW.Location = new System.Drawing.Point(197, 119);
            this.btnResetPW.Name = "btnResetPW";
            this.btnResetPW.Size = new System.Drawing.Size(125, 23);
            this.btnResetPW.TabIndex = 4;
            this.btnResetPW.Text = "Passwort zurücksetzen";
            this.btnResetPW.UseVisualStyleBackColor = true;
            this.btnResetPW.Visible = false;
            this.btnResetPW.Click += new System.EventHandler(this.btnResetPW_Click);
            // 
            // txtPasswordExp
            // 
            this.txtPasswordExp.AutoSize = true;
            this.txtPasswordExp.Location = new System.Drawing.Point(194, 145);
            this.txtPasswordExp.Name = "txtPasswordExp";
            this.txtPasswordExp.Size = new System.Drawing.Size(165, 13);
            this.txtPasswordExp.TabIndex = 16;
            this.txtPasswordExp.Text = "Passwort läuft am 18.06.2018 ab.";
            // 
            // txtLoginAtt
            // 
            this.txtLoginAtt.AutoSize = true;
            this.txtLoginAtt.Location = new System.Drawing.Point(194, 245);
            this.txtLoginAtt.Name = "txtLoginAtt";
            this.txtLoginAtt.Size = new System.Drawing.Size(89, 13);
            this.txtLoginAtt.TabIndex = 18;
            this.txtLoginAtt.Text = "Loginversuche: 0";
            // 
            // txtAccountStatus
            // 
            this.txtAccountStatus.AutoSize = true;
            this.txtAccountStatus.Location = new System.Drawing.Point(194, 223);
            this.txtAccountStatus.Name = "txtAccountStatus";
            this.txtAccountStatus.Size = new System.Drawing.Size(40, 13);
            this.txtAccountStatus.TabIndex = 20;
            this.txtAccountStatus.Text = "Status:";
            // 
            // btnUnlockUser
            // 
            this.btnUnlockUser.Location = new System.Drawing.Point(197, 194);
            this.btnUnlockUser.Name = "btnUnlockUser";
            this.btnUnlockUser.Size = new System.Drawing.Size(125, 23);
            this.btnUnlockUser.TabIndex = 21;
            this.btnUnlockUser.Text = "Benutzer entsperren";
            this.btnUnlockUser.UseVisualStyleBackColor = true;
            this.btnUnlockUser.Visible = false;
            this.btnUnlockUser.Click += new System.EventHandler(this.btnUnlockUser_Click);
            // 
            // PersonDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(364, 310);
            this.Controls.Add(this.btnUnlockUser);
            this.Controls.Add(this.txtAccountStatus);
            this.Controls.Add(this.txtLoginAtt);
            this.Controls.Add(this.txtPasswordExp);
            this.Controls.Add(this.btnResetPW);
            this.Controls.Add(this.cmbPersonType);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtPersonalNo);
            this.Controls.Add(this.txtNickName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.lblPersonType);
            this.Controls.Add(this.lblNickName);
            this.Controls.Add(this.lblLastName);
            this.Controls.Add(this.lblFirstName);
            this.Controls.Add(this.lblPersonalNo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "PersonDataForm";
            this.Text = "Benutzer bearbeiten";
            this.Load += new System.EventHandler(this.PersonDataForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblPersonalNo;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblNickName;
        private System.Windows.Forms.Label lblPersonType;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtNickName;
        private System.Windows.Forms.TextBox txtPersonalNo;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.ComboBox cmbPersonType;
        private System.Windows.Forms.Button btnResetPW;
        private System.Windows.Forms.Label txtPasswordExp;
        private System.Windows.Forms.Label txtLoginAtt;
        private System.Windows.Forms.Label txtAccountStatus;
        private System.Windows.Forms.Button btnUnlockUser;
    }
}