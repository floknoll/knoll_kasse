﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class PersonDataForm : Form
    {
        private Person person = null;

        public PersonDataForm()
        {
            InitializeComponent();
        }

        public PersonDataForm(Person person)
        {
            InitializeComponent();
            this.person = person;
            this.btnResetPW.Visible = true;
            if (this.person.AccountState == AccountStateEnum.Gesperrt)
            {
                this.btnUnlockUser.Visible = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveUser())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Kein Datensatz betroffen!");
        }

        private void PersonDataForm_Load(object sender, EventArgs e)
        {
            this.cmbPersonType.DataSource = Enum.GetValues(typeof(PersonTypeEnum));
            if (this.person != null)
            {
                this.txtPersonalNo.Text = this.person.PersonalNo.ToString();
                this.txtFirstName.Text = this.person.FirstName;
                this.txtLastName.Text = this.person.LastName;
                this.txtNickName.Text = this.person.NickName;
                this.txtPasswordExp.Text = string.Format("Passwort läuft am {0:dd.MM.yyyy} ab.", this.person.PasswordExpiration);
                this.txtLoginAtt.Text = string.Format("Loginversuche: {0}", this.person.LogonAttempt);
                this.txtAccountStatus.Text = string.Format("Status: {0}", this.person.AccountStateText);
                this.cmbPersonType.SelectedItem = this.person.PersonType;                
            }
            else
            {
                this.txtPasswordExp.Text = string.Format("Passwort läuft am {0:dd.MM.yyyy} ab.", DateTime.Now.AddYears(2));
                this.txtAccountStatus.Text = "Status: Neu";
                this.txtLoginAtt.Text = "Loginversuche: 0";
            }         

            this.txtFirstName.Focus();
        }

        private bool SaveUser()
        {
            if (this.person == null)
            {
                this.person = new Person();
                this.person.PasswordExpiration = DateTime.Now.AddYears(2);
                this.person.AccountState = AccountStateEnum.Neu;
                this.person.Password = Crypt.HashSHAPassword(txtNickName.Text);
                this.person.LogonAttempt = 0;
            }
            this.person.FirstName = this.txtFirstName.Text;
            this.person.LastName = this.txtLastName.Text;
            this.person.NickName = this.txtNickName.Text;
            this.person.PersonTypeText = this.cmbPersonType.SelectedValue.ToString();
            
            return this.person.Save();
        }

        private void btnUnlockUser_Click(object sender, EventArgs e)
        {
            this.person.AccountState = AccountStateEnum.Offen;
            this.person.LogonAttempt = 0;
            this.txtLoginAtt.Text = "Loginversuche: 0";
            this.txtAccountStatus.Text = this.person.AccountStateText;
            this.btnUnlockUser.Visible = false;
            this.person.Save();
        }

        private void btnResetPW_Click(object sender, EventArgs e)
        {
            this.person.AccountState = AccountStateEnum.Abgelaufen;
            this.person.LogonAttempt = 0;
            this.txtAccountStatus.Text = this.person.AccountStateText;
            this.txtLoginAtt.Text = "Loginversuche: 0";
            this.person.Save();
        }
    }
}
