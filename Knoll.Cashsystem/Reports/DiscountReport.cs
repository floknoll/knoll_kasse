﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class DiscountReport
    {
        public List<DiscountGrantedItem> DiscountGrantedItems { get; set; }
        public List<DiscountGranted> DiscountsGranted { get; set; }

        public void FillDiscounts()
        {
            DiscountGrantedItems = new List<DiscountGrantedItem>();            
            foreach (var discount in DiscountsGranted)
            {
                DiscountGrantedItem dgi = new DiscountGrantedItem();
                dgi.DiscountGranted = discount;
                dgi.Article = discount.Article;
                dgi.Invoice = discount.Invoice;
                dgi.Discount = discount.ArticleDiscount;
                DiscountGrantedItems.Add(dgi);
            }
        }
    }

    public class DiscountGrantedItem
    {
        public DiscountGranted DiscountGranted { get; set; }
        public Article Article { get; set; }
        public Discount Discount { get; set; }
        public Invoice Invoice { get; set; }

        public DateTime? InvoiceDate
        {
            get
            {
                return Invoice.InvoiceDate;
            }
        }

        public decimal InvoiceNo
        {
            get
            {
                return Invoice.InvoiceNo;
            }
        }

        public decimal ArticleNo
        {
            get
            {
                return Article.ArticleNo;
            }
        }

        public string ArticleDescription
        {
            get
            {
                return Article.Description;
            }
        }

        public string DiscountDescription
        {
            get
            {
                return Discount.Description;
            }
        }

    }
}
