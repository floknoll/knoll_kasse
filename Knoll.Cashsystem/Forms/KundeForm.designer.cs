﻿namespace Knoll.Cashsystem
{
    partial class Kunde
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewArtikelKunde = new System.Windows.Forms.ListView();
            this.columnArtikel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMenge = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPreis = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnGesPreis = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.lblTotalTxt = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // listViewArtikelKunde
            // 
            this.listViewArtikelKunde.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewArtikelKunde.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnArtikel,
            this.columnMenge,
            this.columnPreis,
            this.columnGesPreis});
            this.listViewArtikelKunde.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.listViewArtikelKunde.Location = new System.Drawing.Point(4, 111);
            this.listViewArtikelKunde.Name = "listViewArtikelKunde";
            this.listViewArtikelKunde.Size = new System.Drawing.Size(491, 391);
            this.listViewArtikelKunde.TabIndex = 43;
            this.listViewArtikelKunde.UseCompatibleStateImageBehavior = false;
            this.listViewArtikelKunde.View = System.Windows.Forms.View.Details;
            // 
            // columnArtikel
            // 
            this.columnArtikel.Text = "Artikel";
            this.columnArtikel.Width = 271;
            // 
            // columnMenge
            // 
            this.columnMenge.Text = "Menge";
            this.columnMenge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // columnPreis
            // 
            this.columnPreis.Text = "Preis";
            this.columnPreis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnPreis.Width = 72;
            // 
            // columnGesPreis
            // 
            this.columnGesPreis.Text = "Gesamtpreis";
            this.columnGesPreis.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnGesPreis.Width = 75;
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalPrice.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPrice.Location = new System.Drawing.Point(362, 506);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(133, 23);
            this.lblTotalPrice.TabIndex = 45;
            this.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalTxt
            // 
            this.lblTotalTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalTxt.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalTxt.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTxt.Location = new System.Drawing.Point(286, 505);
            this.lblTotalTxt.Name = "lblTotalTxt";
            this.lblTotalTxt.Size = new System.Drawing.Size(87, 21);
            this.lblTotalTxt.TabIndex = 44;
            this.lblTotalTxt.Text = "Gesamt:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Knoll.Cashsystem.Properties.Resources.Knoll_Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(95, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(278, 102);
            this.pictureBox1.TabIndex = 46;
            this.pictureBox1.TabStop = false;
            // 
            // Kunde
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(498, 534);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblTotalPrice);
            this.Controls.Add(this.lblTotalTxt);
            this.Controls.Add(this.listViewArtikelKunde);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(885, 20);
            this.Name = "Kunde";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Kunde";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListView listViewArtikelKunde;
        private System.Windows.Forms.ColumnHeader columnArtikel;
        private System.Windows.Forms.ColumnHeader columnMenge;
        private System.Windows.Forms.ColumnHeader columnPreis;
        private System.Windows.Forms.ColumnHeader columnGesPreis;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.Label lblTotalTxt;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}