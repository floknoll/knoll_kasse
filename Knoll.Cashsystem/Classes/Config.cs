﻿using Knoll.Cashsystem.Properties;
using Knoll.Cashsystem.Reports;
using Knoll.Data.Cashsystem;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Classes
{
    public class Config
    {
        public const string qrCodePath = @"C:\temp\";

        public const string billQrCodeName = "billQrCode.png";
        public const string userQrCodeName = "userQRCode.png";

        public static string GetUserQRPath
        {
            get
            {
                return qrCodePath + userQrCodeName;
            }
        }

        public static string GetBillQRPath
        {
            get
            {
                return qrCodePath + billQrCodeName;
            }
        }

        public static void PrintUserCard(Person person, string password)
        {
            List<UserCardItem> persons = new List<UserCardItem>();
            UserCardItem userCard = new UserCardItem();
            userCard.Person = person;
            userCard.QRCodeArray = GenerateUserBarcode(person, password);
            persons.Add(userCard);

            LocalReport report = new LocalReport();
            report.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.UserCard.rdlc";
            report.DataSources.Add(new ReportDataSource("PersonDataSet", persons));
            report.EnableExternalImages = true;

            PrintReport printReport = new PrintReport();
            printReport.Run(report, "Mitarbeiterausweis");
        }

        private static byte[] GenerateUserBarcode(Person person, string password)
        {
            var settings = new Spire.Barcode.BarcodeSettings();
            settings.Type = Spire.Barcode.BarCodeType.QRCode;
            settings.Data = "USR:" + person.NickName + ":" + password;
            Spire.Barcode.BarCodeGenerator generator = new Spire.Barcode.BarCodeGenerator(settings);
            Image qrCode = generator.GenerateImage();
            Bitmap bmp = new Bitmap(qrCode);
            Graphics g = Graphics.FromImage(bmp);
            Image empty = Resources.empty;
            g.DrawImage(empty, 0, 0);
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }

        public static void PrintCoupon(Coupon coupon)
        {
            List<CeditNoteItem> creditNotes = new List<CeditNoteItem>();
            CeditNoteItem creditNote = new CeditNoteItem();
            creditNote.Coupon = coupon;
            creditNote.QRCodeArray = GenerateCouponBarcode(coupon);
            creditNotes.Add(creditNote);

            LocalReport report = new LocalReport();
            report.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.CreditNote.rdlc";
            report.DataSources.Add(new ReportDataSource("CreditNoteDataSet", creditNotes));
            report.EnableExternalImages = true;

            PrintReport printReport = new PrintReport();
            printReport.Run(report, "Gutschein");
        }

        private static byte[] GenerateCouponBarcode(Coupon coupon)
        {
            var settings = new Spire.Barcode.BarcodeSettings();
            settings.Type = Spire.Barcode.BarCodeType.QRCode;
            settings.Data = "GUT:" + coupon.SerialNo + ":" + coupon.Spender + ":" + coupon.Value;
            Spire.Barcode.BarCodeGenerator generator = new Spire.Barcode.BarCodeGenerator(settings);
            Image qrCode = generator.GenerateImage();
            Bitmap bmp = new Bitmap(qrCode);
            Graphics g = Graphics.FromImage(bmp);
            Image empty = Resources.empty;
            g.DrawImage(empty, 0, 0);
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
    }
}
