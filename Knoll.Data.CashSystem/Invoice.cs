﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
	public class Invoice : KnollDataObject
	{

		const string selectCmd = "select INVOICE_ID, INVOICE_NO, nettoTaxA, nettoTaxB, BRUTTO, INVOICE_DATE, back_money from knoll_kasse.Invoice";
		const string insertCmd = "insert into knoll_kasse.Invoice (INVOICE_ID, INVOICE_NO, nettoTaxA, nettoTaxB, BRUTTO, INVOICE_DATE, back_money) values (:invoice_id, :invoice_no, :nettoTaxA, :nettoTaxB, :brutto, :invoice_date, :back_money)";
		const string updateCmd = "update knoll_kasse.Invoice set invoice_no = :invoice_no, nettoTaxA = :nettoTaxA, nettoTaxB = :nettoTaxB, brutto = :brutto, invoice_date = :invoice_date, back_money = :back_money where invoice_id = :invoice_id";
		const string deleteCmd = "delete from knoll_kasse.Invoice where invoice_id = :invoice_id";
		//***************************************************************
		#region static methods
            
		public static List<Invoice> GetList() => GetList<Invoice>($"{selectCmd} order by invoice_no");

        public static Invoice Get(decimal invoiceId)
        {
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = $"{selectCmd} where invoice_id = :invoice_id";
            command.Parameters.AddWithValue("invoice_id", invoiceId);
            NpgsqlDataReader reader = command.ExecuteReader();

            Invoice result = null;

            if (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                result = new Invoice(row);
            }
            reader.Close();

            KnollConnection.GetInstance().Connection.Close();
            return result;
        }

        #endregion

        //***************************************************************
        #region constructors
        public Invoice() : base()
		{
            SetValue("invoice_no", -1m);
		}

		public Invoice(KnollDataObject parent) : base(parent)
		{
			SetValue("invoice_no", -1m);
		}

		public Invoice(Dictionary<string, object> row) : base(row)
		{

		}

		public Invoice(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
		{

		}
		#endregion

		//***************************************************************
		#region properties
		public decimal InvoiceId
		{
			get
			{
				return GetValue<decimal>("invoice_no");
			}
		}

		public decimal InvoiceNo
		{
			get
			{
				return GetValue<decimal>("invoice_no");
			}
			set
			{
				SetValue("invoice_no", value);
			}
		}

		public decimal NettoA
		{
			get
			{
				return GetValue<decimal>("nettotaxa");
			}
			set
			{
				SetValue("nettotaxa", value);
			}
		}

        public decimal NettoB
        {
            get
            {
                return GetValue<decimal>("nettotaxb");
            }
            set
            {
                SetValue("nettotaxb", value);
            }
        }

        public decimal Brutto
		{
			get
			{
				return GetValue<decimal>("brutto");
			}
			set
			{
				SetValue("brutto", value);
			}
		}

		public DateTime? InvoiceDate
		{
			get
			{
				return GetValue<DateTime?>("invoice_date");
			}
			set
			{
				SetValue("invoice_date", value);
			}
		}

        public decimal BackMoney
        {
            get
            {
                return GetValue<decimal>("back_money");
            }
            set
            {
                SetValue("back_money", value);
            }
        }

        private List<InvoiceLine> invoiceLines = null;
		public InvoiceLine[] InvliceLines
		{
			get
			{
				if (this.invoiceLines == null) this.invoiceLines = InvoiceLine.GetList(this);
				return this.invoiceLines.ToArray();
			}
		}

        #endregion

        //***************************************************************
        #region public methods

        public InvoiceLine InvoiceLineAdd(decimal iD, int amount, decimal price, int position, string description, string tax, bool isCoupon)
        {
            InvoiceLine invoiceLine = null;
            if (isCoupon)
            {
                invoiceLine = new InvoiceLine(this) { CouponId = iD, Amount = amount, Price = price, Position = position, Description = description };
            }
            else
            {
                 invoiceLine = new InvoiceLine(this) { ArticleId = iD, Amount = amount, Price = price, Position = position, Description = description, Tax = tax };
            }
            if (this.invoiceLines == null) this.invoiceLines = InvoiceLine.GetList(this);
            this.invoiceLines.Add(invoiceLine);
            return invoiceLine;
        }

        public void AccountLogRemove(InvoiceLine invoiceLine)
        {
            invoiceLine.Delete();
            this.invoiceLines.Remove(invoiceLine);
        }

        public bool Save()
		{
            int result = 0;
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlTransaction transaction = KnollConnection.GetInstance().Connection.BeginTransaction();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.Transaction = transaction;
            try
            {
                if (this.InvoiceId == -1)
                {
                    command.CommandText = "select nextval('knoll_kasse.invoice_seq')";
                    SetValue("invoice_id", (decimal)((long)command.ExecuteScalar()));
                    command.CommandText = "select nextval('knoll_kasse.INVOICE_NO_SEQ')";
                    SetValue("invoice_no", (decimal)((long)command.ExecuteScalar()));                    
                    command.CommandText = insertCmd;
                }
                else
                {
                    command.CommandText = updateCmd;
                }

                command.Parameters.AddWithValue("invoice_id", this.InvoiceId);
			    command.Parameters.AddWithValue("invoice_no", this.InvoiceNo);
			    command.Parameters.AddWithValue("nettoTaxA", this.NettoA);
                command.Parameters.AddWithValue("nettoTaxB", this.NettoB);
                command.Parameters.AddWithValue("brutto", this.Brutto);
			    command.Parameters.AddWithValue("invoice_date", this.InvoiceDate);
                command.Parameters.AddWithValue("back_money", this.BackMoney);
                
                result = command.ExecuteNonQuery();

                if (result == 1)
                {
                    if (this.invoiceLines != null && this.invoiceLines.Count > 0)
                    {
                        this.invoiceLines.ToList().ForEach(al => al.Save(transaction));
                    }
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception("Error on db operation", ex);
            }
            finally
            {
                KnollConnection.GetInstance().Connection.Close();
            }

            return result == 1;
        }
    

		public bool Delete()
		{
			KnollConnection.GetInstance().Connection.Open();

			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.CommandText = deleteCmd;
			command.Parameters.AddWithValue("invoice_id", this.InvoiceId);
			int result = command.ExecuteNonQuery();
			KnollConnection.GetInstance().Connection.Close();
			return result == 1;

		}

		#endregion


	}
}
