﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem
{
    public class Product
    {
        public string Barcode { get; set; }
        public decimal? ArticleNo { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal DiscountGranted { get; set; }
        public bool IsCoupon { get; set; }
    }
}
