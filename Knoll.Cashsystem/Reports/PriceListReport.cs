﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class PriceListReport
    {
        public List<PriceListItem> Articles { get; set; }
    }

    public class PriceListItem
    {
        public Article Article { get; set; }

        public decimal ArticleNo
        {
            get
            {
                return Article.ArticleNo;
            }                
        }

        public string Barcode
        {
            get
            {
                return Article.Barcode;
            }
        }

        public decimal Price
        {
            get
            {                
                return Article.Price;
            }
        }

        public decimal? DiscountPrice
        {
            get
            {
                if (Article.ArticleDiscount != null)
                {
                    if (!Article.ArticleDiscount.OnePlusOne && Article.ArticleDiscount.Percent.HasValue && Article.ArticleDiscount.Percent.Value > 0)
                    {
                        decimal percent = 100 - Article.ArticleDiscount.Percent.Value;
                        description = string.Format("{0}  -{1}%", Article.Description, Article.ArticleDiscount.Percent.Value);
                        return (Article.Price / 100) * percent;
                    }
                }
                if (Article.ArticleCategoryDiscount != null)
                {
                    if (!Article.ArticleCategoryDiscount.OnePlusOne && Article.ArticleCategoryDiscount.Percent.HasValue && Article.ArticleCategoryDiscount.Percent.Value > 0)
                    {
                        decimal percent = 100 - Article.ArticleCategoryDiscount.Percent.Value;
                        description = string.Format("{0}  -{1}%", Article.Description, Article.ArticleCategoryDiscount.Percent.Value);
                        return (Article.Price / 100) * percent;
                    }
                }
                return null;
            }
        }

        public string description;
        public string Description
        {
            get
            {
                decimal p = Price;
                if (!string.IsNullOrEmpty(description)) { return description; }
                return Article.Description;
            }
        }
    }               
}
