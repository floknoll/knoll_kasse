﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class DiscountDataForm : Form
    {
        private Discount discount = null;

        public DiscountDataForm()
        {
            InitializeComponent();
        }

        public DiscountDataForm(Discount discount)
        {
            InitializeComponent();
            this.discount = discount;
        }

        private void DiscountDataForm_Load(object sender, EventArgs e)
        {
            if (this.discount != null)
            {
                this.txtDescription.Text = this.discount.Description;
                this.txtPercent.Text = this.discount.Percent.ToString();
                this.chkOnePlusOne.Checked = this.discount.OnePlusOne;
            }
            this.txtDescription.Focus();
        }

        private void chkOnePlusOne_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOnePlusOne.Checked)
            {
                txtPercent.Text = null;
                txtPercent.Enabled = false;
            }
            else
            {
                txtPercent.Enabled = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveDiscount())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Kein Datensatz betroffen!");
        }

        private bool SaveDiscount()
        {
            if (this.discount == null)
            {
                this.discount = new Discount();
            }
            this.discount.Description = this.txtDescription.Text;
            if (!string.IsNullOrEmpty(this.txtPercent.Text))
            {
                this.discount.Percent = decimal.Parse(this.txtPercent.Text);
            }
            this.discount.OnePlusOne = this.chkOnePlusOne.Checked;

            return this.discount.Save();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
