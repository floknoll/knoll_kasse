﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
    public class Article : KnollDataObject
    {

        const string selectCmd = "select ARTICLE_ID, article_category_id, DESCRIPTION, ARTICLE_NO, PRICE, STOCK, barcode, discount_id from knoll_kasse.Article";
        const string insertCmd = "insert into knoll_kasse.Article (ARTICLE_ID, article_category_id, ARTICLE_NO, DESCRIPTION, PRICE, STOCK, barcode, discount_id) values (:article_id, :article_category_id, :article_no, :description, :price, :stock, :barcode, :discount_id)";
        const string updateCmd = "update knoll_kasse.Article set article_no = :article_no, article_category_id = :article_category_id, description = :description, price = :price, stock = :stock, barcode = :barcode, discount_id = :discount_id where article_id = :article_id";
        const string deleteCmd = "delete from knoll_kasse.Article where article_id = :article_id";
        //***************************************************************
        #region static methods

        public static List<Article> GetList() => GetList<Article>($"{selectCmd} order by article_no");

        public static List<Article> GetListLike(string articleNo, string description, string barcode)
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            //if (articleNo == 0)
            //{
                command.CommandText = $"{selectCmd} where CAST(ARTICLE_NO AS TEXT) like '%{articleNo}%' and DESCRIPTION like '%{description}%' and barcode like '%{barcode}%' order by article_no";
            //}
            NpgsqlDataReader reader = command.ExecuteReader();

            List<Article> rows = new List<Article>();
            Dictionary<string, object> row = null;
            while (reader.Read())
            {
                row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                rows.Add((Article)Activator.CreateInstance(typeof(Article), row));
            }
            reader.Close();
            KnollConnection.GetInstance().Connection.Close();
            if (KnollConnection.GetInstance().Connection.State == System.Data.ConnectionState.Open)
                Debug.WriteLine("offen!!!!");
            return rows;
        }

        public static Article Get(string barcode)
        {
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = "select * from knoll_kasse.Article where barcode = :barcode";
            command.Parameters.AddWithValue("barcode", barcode);
            NpgsqlDataReader reader = command.ExecuteReader();

            Article result = null;

            if (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                result = new Article(row);
            }
            reader.Close();

            KnollConnection.GetInstance().Connection.Close();
            return result;
        }

        public static Article Get(decimal articleId)
        {
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = "select * from knoll_kasse.Article where article_id = :article_id";
            command.Parameters.AddWithValue("article_id", articleId);
            NpgsqlDataReader reader = command.ExecuteReader();

            Article result = null;

            if (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                result = new Article(row);
            }
            reader.Close();

            KnollConnection.GetInstance().Connection.Close();
            return result;
        }

        

        
        #endregion

        //***************************************************************
        #region constructors
        public Article() : base()
        {
            SetValue("article_id", -1m);
        }

        public Article(KnollDataObject parent) : base(parent)
        {
            SetValue("article_id", -1m);
        }

        public Article(Dictionary<string, object> row) : base(row)
        {

        }

        public Article(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
        {

        }
        #endregion

        //***************************************************************
        #region properties
        public decimal ArticleId
        {
            get
            {
                return GetValue<decimal>("article_id");
            }
        }

        public decimal? ArticleCategoryId
        {
            get
            {
                return GetValue<decimal>("article_category_id");
            }
            set
            {
                SetValue("article_category_id", value);
            }
        }

        public string Description
        {
            get
            {
                return GetValue<string>("description");
            }
            set
            {
                SetValue("description", value);
            }
        }

        public decimal ArticleNo
        {
            get
            {
                return GetValue<decimal>("article_no");
            }
            set
            {
                SetValue("article_no", value);
            }
        }

        public decimal Price
        {
            get
            {
                return GetValue<decimal>("price");
            }
            set
            {
                SetValue("price", value);
            }
        }

        public decimal Stock
        {
            get
            {
                return GetValue<decimal>("stock");
            }
            set
            {
                SetValue("stock", value);
            }
        }
        
        public string Barcode
        {
            get
            {
                return GetValue<string>("barcode");
            }
            set
            {
                SetValue("barcode", value);
            }
        }

        public decimal? DiscountId
        {
            get
            {
                return GetValue<decimal>("discount_id");
            }
            set
            {
                SetValue("discount_id", value);
            }
        }
        
        private ArticleCategory articleCategory = null;
        public ArticleCategory Category
        {
            get
            {
                if (this.ArticleCategoryId.HasValue)
                {
                    if (this.articleCategory == null) this.articleCategory = ArticleCategory.Get(ArticleCategoryId.Value);
                    return this.articleCategory;
                }
                return null;
            }
        }
        private Discount articleDiscount = null;
        public Discount ArticleDiscount
        {
            get
            {                
                if (this.DiscountId.HasValue)
                {                
                    if (this.articleDiscount == null) this.articleDiscount = Discount.Get(DiscountId.Value);
                    return this.articleDiscount;
                }
                return null;
            }
        }

        private Discount articleCategoryDiscount = null;
        public Discount ArticleCategoryDiscount
        {
            get
            {
                if (Category != null)
                {
                    if (this.articleCategoryDiscount == null) this.articleCategoryDiscount = Discount.Get(Category.DiscountId);
                    return this.articleCategoryDiscount;
                }
                return null;
            }
        }

        #endregion

        //***************************************************************
        #region public methods
        public bool Save()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;

            if (this.ArticleId == -1)
            {
                command.CommandText = "select nextval('knoll_kasse.article_seq')";
                SetValue("article_id", (decimal)((long)command.ExecuteScalar()));
                command.CommandText = "select nextval('knoll_kasse.article_no_seq')";
                SetValue("article_no", (decimal)((long)command.ExecuteScalar()));                
                command.CommandText = insertCmd;
            }
            else
            {
                command.CommandText = updateCmd;
            }

            command.Parameters.AddWithValue("article_id", this.ArticleId);
            command.Parameters.AddWithValue("article_no", this.ArticleNo);
            command.Parameters.AddWithValue("description", this.Description);
            command.Parameters.AddWithValue("price", this.Price);
            command.Parameters.AddWithValue("stock", this.Stock);
            command.Parameters.AddWithValue("barcode", this.Barcode);
            command.Parameters.AddWithValue("article_category_id", this.ArticleCategoryId.HasValue ? (object)this.ArticleCategoryId.Value : (object)DBNull.Value);
            command.Parameters.AddWithValue("discount_id", this.DiscountId.HasValue ? (object)this.DiscountId.Value : (object)DBNull.Value);       


            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }

        public bool Delete()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = deleteCmd;
            command.Parameters.AddWithValue("article_id", this.ArticleId);
            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;

        }

        #endregion


    }
}
