﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class CouponReport
    {
        public List<Coupon> Coupons { get; set; }
    }
}
