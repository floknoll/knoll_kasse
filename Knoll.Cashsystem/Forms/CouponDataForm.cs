﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class CouponDataForm : Form
    {
        private Coupon coupon = null;

        public CouponDataForm()
        {
            InitializeComponent();
        }

        public CouponDataForm(Coupon coupon)
        {
            InitializeComponent();
            this.coupon = coupon;
        }

        private void CouponDataForm_Load(object sender, EventArgs e)
        {
            if (this.coupon != null)
            {
                this.txtSerialNo.Text = this.coupon.SerialNo;
                this.txtSpender.Text = this.coupon.Spender;
                this.txtValue.Text = string.Format("{0:n}", this.coupon.Value);
                if (this.coupon.Redeemed != DateTime.MinValue)
                { 
                    this.txtRedeemed.Text = string.Format("{0:dd.MM.yyyy}", this.coupon.Redeemed);
                }
                this.chkValid.Checked = this.coupon.Valid;
            }
            this.txtSerialNo.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveCoupon())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Kein Datensatz betroffen!");
        }
        
        private bool SaveCoupon()
        {
            if (this.coupon == null)
            {
                this.coupon = new Coupon();
            }
            this.coupon.SerialNo = this.txtSerialNo.Text;
            this.coupon.Spender = this.txtSpender.Text;
            this.coupon.Value = decimal.Parse(this.txtValue.Text);
            if (!string.IsNullOrEmpty(this.txtRedeemed.Text))
            {
                this.coupon.Redeemed = DateTime.Parse(this.txtRedeemed.Text);
            }
            this.coupon.Valid = this.chkValid.Checked;
            return this.coupon.Save();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
