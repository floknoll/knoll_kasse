﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
    public class ArticleCategory : KnollDataObject
    {

        const string selectCmd = "select article_category_id, discount_id, DESCRIPTION, tax from knoll_kasse.Article_Category";
        const string insertCmd = "insert into knoll_kasse.Article_Category (article_category_id, discount_id, DESCRIPTION, tax) values (:article_category_id, :discount_id, :description, :tax)";
        const string updateCmd = "update knoll_kasse.Article_Category set discount_id = :discount_id, description = :description, tax = :tax where article_category_id = :article_category_id";
        const string deleteCmd = "delete from knoll_kasse.Article_Category where article_category_id = :article_category_id";
        //***************************************************************
        #region static methods

        public static List<ArticleCategory> GetList() => GetList<ArticleCategory>($"{selectCmd} order by description");

        public static ArticleCategory Get(decimal categoryId)
        {
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = "select * from knoll_kasse.Article_Category where article_category_id = :article_category_id";
            command.Parameters.AddWithValue("article_category_id", categoryId);
            NpgsqlDataReader reader = command.ExecuteReader();

            ArticleCategory result = null;

            if (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                result = new ArticleCategory(row);
            }
            reader.Close();

            KnollConnection.GetInstance().Connection.Close();
            return result;
        }

        #endregion

        //***************************************************************
        #region constructors
        public ArticleCategory() : base()
        {
            SetValue("article_category_id", -1m);
        }

        public ArticleCategory(KnollDataObject parent) : base(parent)
        {
            SetValue("article_category_id", -1m);
        }

        public ArticleCategory(Dictionary<string, object> row) : base(row)
        {

        }

        public ArticleCategory(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
        {

        }
        #endregion

        //***************************************************************
        #region properties
        public decimal ArticleCategoryId
        {
            get
            {
                return GetValue<decimal>("article_category_id");
            }
        }

        public decimal DiscountId
        {
            get
            {
                return GetValue<decimal>("discount_id");
            }
            set
            {
                SetValue("discount_id", value);
            }
        }

        public string Description
        {
            get
            {
                return GetValue<string>("description");
            }
            set
            {
                SetValue("description", value);
            }
        }

        public string Tax
        {
            get
            {
                return GetValue<string>("tax");
            }
            set
            {
                SetValue("tax", value);
            }
        }

        #endregion

        //***************************************************************
        #region public methods
        public bool Save()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;

            if (this.ArticleCategoryId == -1)
            {
                command.CommandText = "select nextval('knoll_kasse.article_category_seq')";
                SetValue("article_category_id", (decimal)((long)command.ExecuteScalar()));               
                command.CommandText = insertCmd;
            }
            else
            {
                command.CommandText = updateCmd;
            }

            command.Parameters.AddWithValue("article_category_id", this.ArticleCategoryId);
            command.Parameters.AddWithValue("discount_id", this.DiscountId);
            command.Parameters.AddWithValue("description", this.Description);
            command.Parameters.AddWithValue("tax", this.Tax);

            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }

        public bool Delete()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = deleteCmd;
            command.Parameters.AddWithValue("article_category_id", this.ArticleCategoryId);
            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;

        }

        #endregion


    }
}
