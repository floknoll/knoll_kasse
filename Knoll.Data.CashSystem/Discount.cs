﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
    public class Discount : KnollDataObject
    {

        const string selectCmd = "select discount_id, DESCRIPTION, one_plus_one, percent from knoll_kasse.Discount";
        const string insertCmd = "insert into knoll_kasse.Discount (discount_id, DESCRIPTION, one_plus_one, percent) values (:discount_id, :description, :one_plus_one, :percent)";
        const string updateCmd = "update knoll_kasse.Discount set description = :description, one_plus_one = :one_plus_one, percent = :percent where discount_id = :discount_id";
        const string deleteCmd = "delete from knoll_kasse.Discount where discount_id = :discount_id";
        //***************************************************************
        #region static methods

        public static List<Discount> GetList() => GetList<Discount>($"{selectCmd} order by description");

        public static Discount Get(decimal discountId)
        {
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = "select * from knoll_kasse.Discount where discount_id = :discount_id";
            command.Parameters.AddWithValue("discount_id", discountId);
            NpgsqlDataReader reader = command.ExecuteReader();

            Discount result = null;

            if (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                result = new Discount(row);
            }
            reader.Close();

            KnollConnection.GetInstance().Connection.Close();
            return result;
        }

        #endregion

        //***************************************************************
        #region constructors
        public Discount() : base()
        {
            SetValue("discount_id", -1m);
        }

        public Discount(KnollDataObject parent) : base(parent)
        {
            SetValue("discount_id", -1m);
        }

        public Discount(Dictionary<string, object> row) : base(row)
        {

        }

        public Discount(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
        {

        }
        #endregion

        //***************************************************************
        #region properties
        public decimal DiscountId
        {
            get
            {
                return GetValue<decimal>("discount_id");
            }
        }

        public string Description
        {
            get
            {
                return GetValue<string>("description");
            }
            set
            {
                SetValue("description", value);
            }
        }

        public bool OnePlusOne
        {
            get
            {
                return GetValue<bool>("one_plus_one");
            }
            set
            {
                SetValue("one_plus_one", value);
            }
        }

        public decimal? Percent
        {
            get
            {
                return GetValue<decimal>("percent");
            }
            set
            {
                SetValue("percent", value);
            }
        }

        public string OnePlusOneText
        {
            get
            {
                switch (OnePlusOne)
                {
                    case true:
                        return "Ja";
                    case false:
                        return "Nein";
                    default:
                        break;
                }
                return null;
            }
        }

        #endregion

        //***************************************************************
        #region public methods
        public bool Save()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;

            if (this.DiscountId == -1)
            {
                command.CommandText = "select nextval('knoll_kasse.discount_seq')";
                SetValue("discount_id", (decimal)((long)command.ExecuteScalar()));               
                command.CommandText = insertCmd;
            }
            else
            {
                command.CommandText = updateCmd;
            }

            command.Parameters.AddWithValue("discount_id", this.DiscountId);
            command.Parameters.AddWithValue("description", this.Description);
            command.Parameters.AddWithValue("one_plus_one", this.OnePlusOne);
            command.Parameters.AddWithValue("percent", this.Percent);

            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }

        public bool Delete()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = deleteCmd;
            command.Parameters.AddWithValue("discount_id", this.DiscountId);
            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;

        }

        #endregion


    }
}
