﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
	public class Person : KnollDataObject
	{

		const string selectCmd = "select person_id, first_name, last_name, nick_name, password, password_expiration, personal_no, person_type_text, account_state_text, date_of_birth, logon_attempt, security_question1, security_question2 from knoll_kasse.person";
		const string insertCmd = "insert into knoll_kasse.person (person_id, first_name, last_name, nick_name, password, password_expiration, personal_no, person_type_text, account_state_text, date_of_birth, logon_attempt, security_question1, security_question2) values (:person_id, :firstname, :lastname, :nickname, :password, :passwordexpiration, :personal_no, :persontypetext, :accountstatetext, :dateofbirth, :logonattempt, :security_question1, :security_question2)";
		const string updateCmd = "update knoll_kasse.person set first_name = :firstname, last_name = :lastname, nick_name = :nickname, password = :password, password_expiration = :passwordexpiration, personal_no = :personal_no, person_type_text = :persontypetext, account_state_text = :accountstatetext, date_of_birth = :dateofbirth, logon_attempt = :logonattempt, security_question1 = :security_question1, security_question2 = :security_question2 where person_id = :person_id";
		const string deleteCmd = "delete from knoll_kasse.person where person_id = :person_id";
		//***************************************************************
		#region static methods
            
		public static List<Person> GetList() => GetList<Person>($"{selectCmd} order by personal_no");

		public static Person Get(string user)
		{
			KnollConnection.GetInstance().Connection.Open();
			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.CommandText = "select * from knoll_kasse.person where nick_name = :nickname";
			command.Parameters.AddWithValue("nickname", user);
			NpgsqlDataReader reader = command.ExecuteReader();

			Person result = null;

			if (reader.Read())
			{
				Dictionary<string, object> row =  new Dictionary<string, object>();
				for (int idx = 0; idx < reader.FieldCount; idx++)
				{
					row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
				}
				result = new Person(row);
			}
			reader.Close();

			KnollConnection.GetInstance().Connection.Close();
			return result;
		}
		#endregion

		//***************************************************************
		#region constructors
		public Person() : base()
		{
            SetValue("person_id", -1m);
		}

		public Person(KnollDataObject parent) : base(parent)
		{
			SetValue("person_id", -1m);
		}

		public Person(Dictionary<string, object> row) : base(row)
		{

		}

		public Person(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
		{

		}
		#endregion

		//***************************************************************
		#region properties
		public decimal PersonId
		{
			get
			{
				return GetValue<decimal>("person_id");
			}
		}

		public string FirstName
		{
			get
			{
				return GetValue<string>("first_name");
			}
			set
			{
				SetValue("first_name", value);
			}
		}

		public string LastName
		{
			get
			{
				return GetValue<string>("last_name");
			}
			set
			{
				SetValue("last_name", value);
			}
		}

		public string NickName
		{
			get
			{
				return GetValue<string>("nick_name");
			}
			set
			{
				SetValue("nick_name", value);
			}
		}

		public string Password
		{
			get
			{
				return GetValue<string>("password");
			}
			set
			{
				SetValue("password", value);
			}
		}

		public DateTime? PasswordExpiration
		{
			get
			{
				return GetValue<DateTime?>("password_expiration");
			}
			set
			{
				SetValue("password_expiration", value);
			}
		}

		public decimal? PersonalNo
		{
			get
			{
				return GetValue<decimal?>("personal_no");
			}
			set
			{
				SetValue("personal_no", value);
			}
		}

		public string PersonTypeText
		{
			get
			{
				return GetValue<string>("person_type_text");
			}
			set
			{
				SetValue("person_type_text", value);
			}
		}

		public PersonTypeEnum PersonType
		{
			get
			{
				return (PersonTypeEnum)Enum.Parse(typeof(PersonTypeEnum), this.PersonTypeText);
			}
			set
			{
				this.PersonTypeText = value.ToString();
			}
		}

		public string AccountStateText
		{
			get
			{
				return GetValue<string>("account_state_text");
			}
			set
			{
				SetValue("account_state_text", value);
			}
		}

		public AccountStateEnum AccountState
		{
			get
			{
				return (AccountStateEnum)Enum.Parse(typeof(AccountStateEnum), this.AccountStateText);
			}
			set
			{
				this.AccountStateText = value.ToString();
			}
		}


		public DateTime? DateOfBirth
		{
			get
			{
				return GetValue<DateTime?>("date_of_birth");
			}
			set
			{
				SetValue("date_of_birth", value);
			}
		}

		public decimal? LogonAttempt
		{
			get
			{
				return GetValue<decimal?>("logon_attempt");
			}
			set
			{
				SetValue("logon_attempt", value);
			}
		}

        public string SecurityQuestion1
        {
            get
            {
                return GetValue<string>("security_question1");
            }
            set
            {
                SetValue("security_question1", value);
            }
        }
        public string SecurityQuestion2
        {
            get
            {
                return GetValue<string>("security_question2");
            }
            set
            {
                SetValue("security_question2", value);
            }
        }

        private List<AccountLog> accountLogs = null;
		public AccountLog[] AccountLogs
		{
			get
			{
				if (this.accountLogs == null) this.accountLogs = AccountLog.GetList(this);
				return this.accountLogs.ToArray();
			}
		}

        #endregion

        //***************************************************************
        #region public methods

        public AccountLog AccountLogAdd(DateTime logonDate, bool success)
        {
            AccountLog accountLog = new AccountLog(this) { LastLogon = logonDate, Success = success, IsNew = true };
            if (this.accountLogs == null) this.accountLogs = AccountLog.GetList(this);
            this.accountLogs.Add(accountLog);
            return accountLog;
        }

        public void AccountLogRemove(AccountLog accountLog)
        {
            accountLog.Delete();
            this.accountLogs.Remove(accountLog);
        }

        public bool Save()
		{
            int result = 0;
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlTransaction transaction = KnollConnection.GetInstance().Connection.BeginTransaction();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.Transaction = transaction;
            try
            {
                if (this.PersonId == -1)
                {
                    command.CommandText = "select nextval('knoll_kasse.person_seq')";
                    SetValue("person_id", (decimal)((long)command.ExecuteScalar()));
                    command.CommandText = "select nextval('knoll_kasse.personal_no_seq')";
                    SetValue("personal_no", (decimal)((long)command.ExecuteScalar()));
                    command.CommandText = insertCmd;
                }
                else
                {
                    command.CommandText = updateCmd;
                }

                command.Parameters.AddWithValue("person_id", this.PersonId);
			    command.Parameters.AddWithValue("firstname", this.FirstName);
			    command.Parameters.AddWithValue("lastname", this.LastName);
			    command.Parameters.AddWithValue("nickname", this.NickName);
			    command.Parameters.AddWithValue("password", this.Password);
			    command.Parameters.AddWithValue("passwordexpiration", this.PasswordExpiration.HasValue ? (object)this.PasswordExpiration.Value : (object)DBNull.Value);
			    command.Parameters.AddWithValue("personal_no", this.PersonalNo.HasValue ? (object)this.PersonalNo.Value : (object)DBNull.Value);
			    command.Parameters.AddWithValue("persontypetext", String.IsNullOrEmpty(this.PersonTypeText) ? (object)DBNull.Value : (object)this.PersonTypeText);
			    command.Parameters.AddWithValue("accountstatetext", String.IsNullOrEmpty(this.AccountStateText) ? (object)DBNull.Value : (object)this.AccountStateText);
			    command.Parameters.AddWithValue("dateofbirth", this.DateOfBirth.HasValue ? (object)this.DateOfBirth.Value : (object)DBNull.Value);
			    command.Parameters.AddWithValue("logonattempt", this.LogonAttempt.HasValue ? (object)this.LogonAttempt.Value : (object)DBNull.Value);
                command.Parameters.AddWithValue("security_question1", String.IsNullOrEmpty(this.SecurityQuestion1) ? (object)DBNull.Value : (object)this.SecurityQuestion1);
                command.Parameters.AddWithValue("security_question2", String.IsNullOrEmpty(this.SecurityQuestion2) ? (object)DBNull.Value : (object)this.SecurityQuestion2);

                result = command.ExecuteNonQuery();

                if (result == 1)
                {
                    if (this.accountLogs != null && this.accountLogs.Count > 0)
                    {
                        this.accountLogs.Where(al => al.IsNew).ToList().ForEach(al => al.Save(transaction));
                    }
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception("Error on db operation", ex);
            }
            finally
            {
                KnollConnection.GetInstance().Connection.Close();
            }

            return result == 1;
        }
    

		public bool Delete()
		{
			KnollConnection.GetInstance().Connection.Open();

			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.CommandText = deleteCmd;
			command.Parameters.AddWithValue("person_id", this.PersonId);
			int result = command.ExecuteNonQuery();
			KnollConnection.GetInstance().Connection.Close();
			return result == 1;

		}

		#endregion


	}
}
