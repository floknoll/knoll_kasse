﻿using Knoll.BarcodeListener;
using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class ArticleDataForm : Form
    {
        private Article article = null;
        BcListener listener = null;

        public ArticleDataForm()
        {
            InitializeComponent();
            // Barcode scan
        }

        public ArticleDataForm(Article article)
        {
            InitializeComponent();
            this.article = article;
        }

        private void ArticleDataForm_Load(object sender, EventArgs e)
        {            
            List<ArticleCategory> categorys = ArticleCategory.GetList();
            this.cmbArticleCategory.DataSource = categorys;
            cmbArticleCategory.DisplayMember = "DESCRIPTION";
            cmbArticleCategory.ValueMember = "ArticleCategoryId";
            List<Discount> discounts = Discount.GetList();
            this.cmbDiscount.DataSource = discounts;
            cmbDiscount.DisplayMember = "Description";
            cmbDiscount.ValueMember = "DiscountId";
            if (this.article != null)
            {
                this.txtArticleNo.Text = this.article.ArticleNo.ToString();
                this.txtDescription.Text = this.article.Description;
                this.txtPrice.Text = string.Format("{0:n}", this.article.Price);
                this.txtStock.Text = this.article.Stock.ToString();
                this.txtBarcode.Text = this.article.Barcode;
                this.cmbArticleCategory.SelectedValue = this.article.ArticleCategoryId;
                this.cmbDiscount.SelectedValue = this.article.DiscountId;
            }
            else
            {
                this.btnScanArticle.Visible = true;
            }

            this.txtArticleNo.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.txtBarcode.Text != "Bereit zum scannen!")
            {
                if (SaveArticle())
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                    MessageBox.Show("Kein Datensatz betroffen!");
            }
            else
            {
                MessageBox.Show("Bitte geben Sie einen Barcode an!");
            }
        }        

        private bool SaveArticle()
        {
            if (this.article == null) this.article = new Article();
            this.article.Barcode = this.txtBarcode.Text;
            this.article.Description = this.txtDescription.Text;
            this.article.Price = decimal.Parse(this.txtPrice.Text);
            this.article.Stock = decimal.Parse(this.txtStock.Text);
            this.article.ArticleCategoryId = (decimal)this.cmbArticleCategory.SelectedValue;
            this.article.DiscountId = (decimal)this.cmbDiscount.SelectedValue;
            return this.article.Save();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ArticleDataForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, e);
            }
            if (e.KeyCode == Keys.Enter)
            {
                btnOK_Click(sender, e);
            }
        }

        private void btnScanArticle_Click(object sender, EventArgs e)
        {            
            listener = BcListener.GetBarcodeListener();
            listener.OnProduktBarcodeDataReceived += Listener_OnProduktBarcodeDataReceived;
            this.txtBarcode.Text = "Bereit zum scannen!";
        }

        private void Listener_OnProduktBarcodeDataReceived(object sender, EventArgs e)
        {
            Invoke(new Action(() =>
            {
                var barcode = (e as ProduktEventArgs).Barcode.Split(':');
                if (barcode[0] != null)
                {
                    this.txtBarcode.Text = barcode[0];
                }
                else
                {
                    MessageBox.Show("Falscher Barcode! Bitte versuchen Sie es erneut.");
                }
            }));
        }

        private void ArticleDataForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (listener != null)
            {
                listener.OnProduktBarcodeDataReceived -= Listener_OnProduktBarcodeDataReceived;
            }
        }
    }
}
