﻿using Knoll.SocketListener;
using System;
using static Knoll.SocketListener.Listener;

namespace Knoll.BarcodeListener
{
    public class LoginEventArgs : EventArgs
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class ProduktEventArgs : EventArgs
    {
        public string Barcode { get; set; }
    }

    public class ArticleEventArgs : EventArgs
    {
        public string Barcode { get; set; }
    }

    public class BcListener
    {
        public event EventHandler OnLoginDataReceived = delegate { };
        public event EventHandler OnProduktBarcodeDataReceived = delegate { };

        private static BcListener instance = null;

        private BcListener()
        {
            StartListening();
        }

        Listener listener = null;

        private void StartListening()
        {
            listener = new Listener();
            listener.OnMessageReceived += Listener_OnMessageReceived;
        }

        private void Listener_OnMessageReceived(object sender, EventArgs e)
        {
            if (e is AndroidMessage androidMessage)
            {
                if (androidMessage.Message.Contains("USR:"))
                {
                    var elements = androidMessage.Message.Split(':');
                    OnLoginDataReceived(this, new LoginEventArgs { UserName = elements[1], Password = elements[2] });
                }
                else
                {
                    OnProduktBarcodeDataReceived(this, new ProduktEventArgs { Barcode = androidMessage.Message });
                }
            }
        }

        public static BcListener GetBarcodeListener()
        {
            if (instance == null)
            {
                instance = new BcListener();
            }
            return instance;
        }
    }
}
