﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem
{
    class Crypt
    {
        public static string HashSHAPassword(string password)
        {

            byte[] arrbyte = new byte[password.Length];
            SHA256 hash = new SHA256CryptoServiceProvider();
            arrbyte = hash.ComputeHash(Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(arrbyte);
        }
    }
}
