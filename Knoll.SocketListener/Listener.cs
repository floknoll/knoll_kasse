﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Knoll.SocketListener
{
    public class Listener
    {        
        public class AndroidMessage : EventArgs
        {
            public string Message { get; set; }
        }

        //public event EventHandler OnMessageReceived = delegate { };
        public event EventHandler OnMessageReceived;

        private Thread listenerThread = null;

        public Listener()
        {
            listenerThread = new Thread(new ThreadStart(Listen));
            listenerThread.IsBackground = true;
            listenerThread.ApartmentState = ApartmentState.STA;
            listenerThread.Start();
        }

        private void Listen()
        {
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAdress = ipHostInfo.AddressList[0];

            IPEndPoint localEndPoint = new IPEndPoint(ipAdress, 1755);
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);
                string data = null;

                //Start listening
                while (true)
                {
                    Socket handler = listener.Accept();
                    var bytes = new Byte[1024];

                    int bytesReceived = handler.Receive(bytes);
                    var message = Encoding.UTF8.GetString(bytes, 2, bytes[1]);
                    try
                    {
                        OnMessageReceived(this, new AndroidMessage { Message = message });
                    }
                    catch { }
                }

            }
            catch (Exception e) {
                int x = 110;
            }
        }

    }
}
