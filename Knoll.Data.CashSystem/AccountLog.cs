﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
	public class AccountLog : KnollDataObject
	{

		const string sequenceName = "knoll_kasse.account_log_seq";
		const string selectCmd = "select account_log_id, person_id, last_logon, success, message from knoll_kasse.account_log";
		const string insertCmd = "insert into knoll_kasse.account_log (account_log_id, person_id, last_logon, success, message) values (:accountlogid, :personid, :lastlogon, :success, :message)";
		const string updateCmd = "update knoll_kasse.account_log set person_id = :personid, last_logon = :lastlogon, success = :success, message = :message where account_log_id = :accountlogid";
		const string deleteCmd = "delete from knoll_kasse.account_log where account_log_id = :accountlogid";
		//***************************************************************
		#region static methods

		public static List<AccountLog> GetList() => GetList<AccountLog>($"{selectCmd} order by last_logon desc");

		public static List<AccountLog> GetList(Person person) => GetList<AccountLog>(person, $"{selectCmd} where person_id = :persid order by last_logon desc", new KeyValuePair<string, object>("persid", person.PersonId));

		#endregion

		//***************************************************************
		#region constructors
		public AccountLog() : base()
		{

		}

		public AccountLog(KnollDataObject parent) : base(parent)
		{
			SetValue("account_log_id", -1m);
		}

		public AccountLog(Dictionary<string, object> row) : base(row)
		{

		}

		public AccountLog(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
		{

		}
		#endregion

		//***************************************************************
		#region properties
		public decimal AccountLogIid
		{
			get
			{
				return GetValue<decimal>("account_log_id");
			}
		}

		public DateTime? LastLogon
		{
			get
			{
				return GetValue<DateTime?>("last_logon");
			}
			set
			{
				SetValue("last_logon", value);
			}
		}

		public bool Success
		{
			get
			{
				return GetValue<bool>("success");
			}
			set
			{
				SetValue("success", value);
			}
		}

        public string Message
        {
            get
            {
                return GetValue<string>("message");
            }
            set
            {
                SetValue("message", value);
            }
        }
        
        public bool IsNew { get; set; } = false;

		#endregion

		//***************************************************************
		#region public methods
		public bool Save(NpgsqlTransaction transaction = null)
		{
			if (transaction == null)
				KnollConnection.GetInstance().Connection.Open();

			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.Transaction = transaction;
			if (this.AccountLogIid == -1)
			{
				command.CommandText = $"select nextval('{sequenceName}')";
				SetValue("account_log_id", (decimal)((long)command.ExecuteScalar()));
				command.CommandText = insertCmd;
			}
			else
			{
				command.CommandText = updateCmd;
			}

			command.Parameters.AddWithValue("accountlogid", this.AccountLogIid);
			command.Parameters.AddWithValue("personid", ((Person)this.parent).PersonId);
			command.Parameters.AddWithValue("lastlogon", this.LastLogon);
			command.Parameters.AddWithValue("success", this.Success);
            command.Parameters.AddWithValue("message", this.Message);

            int result = command.ExecuteNonQuery();
			
			if (transaction == null)
                KnollConnection.GetInstance().Connection.Close();
			return result == 1;
		}

		public bool Delete()
		{
            KnollConnection.GetInstance().Connection.Open();

			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.CommandText = deleteCmd;
			command.Parameters.AddWithValue("accountlogid", this.AccountLogIid);
			int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
			return result == 1;

		}

		#endregion


	}
}
