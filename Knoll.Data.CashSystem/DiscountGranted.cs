﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
    public class DiscountGranted : KnollDataObject
    {

        const string selectCmd = "select discount_granted_id, article_id, discount_id, invoice_id from knoll_kasse.Discount_Granted";
        const string insertCmd = "insert into knoll_kasse.Discount_Granted (discount_granted_id, article_id, discount_id, invoice_id) values (:discount_granted_id, :article_id, :discount_id, :invoice_id)";
        const string updateCmd = "update knoll_kasse.Discount_Granted set article_id = :article_id, discount_id = :discount_id, invoice_id = :invoice_id where discount_granted_id = :discount_granted_id";
        const string deleteCmd = "delete from knoll_kasse.Discount_Granted where discount_granted_id = :discount_granted_id";
        //***************************************************************
        #region static methods

        public static List<DiscountGranted> GetList() => GetList<DiscountGranted>($"{selectCmd} order by invoice_id");
        
        #endregion

        //***************************************************************
        #region constructors
        public DiscountGranted() : base()
        {
            SetValue("discount_granted_id", -1m);
        }

        public DiscountGranted(KnollDataObject parent) : base(parent)
        {
            SetValue("discount_granted_id", -1m);
        }

        public DiscountGranted(Dictionary<string, object> row) : base(row)
        {

        }

        public DiscountGranted(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
        {

        }
        #endregion

        //***************************************************************
        #region properties
        public decimal DiscountGrantedId
        {
            get
            {
                return GetValue<decimal>("discount_granted_id");
            }
        }

        public decimal ArticleId
        {
            get
            {
                return GetValue<decimal>("article_id");
            }
            set
            {
                SetValue("article_id", value);
            }
        }

        public decimal DiscountId
        {
            get
            {
                return GetValue<decimal>("discount_id");
            }
            set
            {
                SetValue("discount_id", value);
            }
        }

        public decimal InvoiceId
        {
            get
            {
                return GetValue<decimal>("invoice_id");
            }
            set
            {
                SetValue("invoice_id", value);
            }
        }

        private Discount articleDiscount = null;
        public Discount ArticleDiscount
        {
            get
            {
                if (this.articleDiscount == null) this.articleDiscount = Discount.Get(this.DiscountId);
                return this.articleDiscount;
            }
        }

        private Article article = null;
        public Article Article
        {
            get
            {
                if (this.article == null) this.article = Article.Get(this.ArticleId);
                return this.article;
            }
        }

        private Invoice invoice = null;
        public Invoice Invoice
        {
            get
            {
                if (this.invoice == null) this.invoice = Invoice.Get(this.InvoiceId);
                return this.invoice;
            }
        }


        #endregion

        //***************************************************************
        #region public methods
        public bool Save()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;

            if (this.DiscountGrantedId == -1)
            {
                command.CommandText = "select nextval('knoll_kasse.discount_granted_seq')";
                SetValue("discount_granted_id", (decimal)((long)command.ExecuteScalar()));               
                command.CommandText = insertCmd;
            }
            else
            {
                command.CommandText = updateCmd;
            }

            command.Parameters.AddWithValue("discount_granted_id", this.DiscountGrantedId);
            command.Parameters.AddWithValue("article_id", this.ArticleId);
            command.Parameters.AddWithValue("discount_id", this.DiscountId);
            command.Parameters.AddWithValue("invoice_id", this.InvoiceId);

            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }

        public bool Delete()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = deleteCmd;
            command.Parameters.AddWithValue("discount_granted_id", this.DiscountGrantedId);
            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }

        #endregion


    }
}
