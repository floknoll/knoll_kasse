﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public class Login
    {
        public void DoLogin(LoginForm loginForm, Person person)
        {
            AccountLog accountLog = person.AccountLogAdd(DateTime.Now, false);
            if (person.AccountState == AccountStateEnum.Gesperrt)
            {
                MessageBox.Show("Account gesperrt, Bitte wenden Sie sich an einen Administrator!");
            }
            else
            {                
                if (person.Password != Crypt.HashSHAPassword(loginForm.Password) && person.Password != loginForm.Password)
                {
                    MessageBox.Show("Falsches Passwort!");
                    person.LogonAttempt = person.LogonAttempt.HasValue ? person.LogonAttempt.Value + 1 : 1;
                    if (person.LogonAttempt > 3)
                    {
                        person.AccountState = AccountStateEnum.Gesperrt;
                    }
                    accountLog.Success = false;
                    accountLog.Message = "Login";
                    person.Save();
                    //Application.Restart();
                }
                else
                {
                    accountLog.Success = true;
                    accountLog.Message = "Login";
                    person.LogonAttempt = 0;
                    if (person.AccountState == AccountStateEnum.Neu || person.AccountState == AccountStateEnum.Abgelaufen)
                    {
                        if (person.AccountState == AccountStateEnum.Abgelaufen)
                        {
                            MessageBox.Show("Ihr Account ist abgelaufen, bitte geben Sie ein neues Passwort an!");
                            loginForm.ShowPasswordChange();
                        }
                        if (person.AccountState == AccountStateEnum.Neu)
                        {
                            MessageBox.Show("Neuer Account! \nBitte setzen Sie ein neues Passwort und eine Sicherheitsfrage.");
                            loginForm.ShowNewPasswordChange();
                        }
                        loginForm.Cancel = true;
                        if (loginForm.ShowDialog() == DialogResult.Yes)
                        {
                            if (person.AccountState == AccountStateEnum.Neu && !loginForm.ForgotPassword)
                            {
                                person.SecurityQuestion1 = loginForm.SecurityQuestion1;
                                person.SecurityQuestion2 = loginForm.SecurityQuestion2;
                            }
                            person.Password = Crypt.HashSHAPassword(loginForm.NewPassword);
                            person.AccountState = AccountStateEnum.Offen;
                            person.PasswordExpiration = DateTime.Now.AddYears(2);
                            person.LogonAttempt = 0;
                        }
                    }
                    person.Save();
                    loginForm.SetInVisible();
                    if (person.PersonType == PersonTypeEnum.Admin)
                    {
                        AdminForm adminForm = new AdminForm(loginForm);
                        adminForm.ShowDialog();
                        //Application.Run(new AdminForm());
                    }
                    if (person.PersonType == PersonTypeEnum.Kassier)
                    {
                        KassenForm kassenForm = new KassenForm(loginForm);
                        kassenForm.ShowDialog();
                        //Application.Run(new KassenForm());
                    }
                }
            }
        }
    }
}
