﻿namespace Knoll.Cashsystem
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource5 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.SalesReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ProductReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CouponReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DiscountReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PriceListReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControlAdmin = new System.Windows.Forms.TabControl();
            this.tabPageUser = new System.Windows.Forms.TabPage();
            this.listViewUser = new System.Windows.Forms.ListView();
            this.columnPersonalNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnFirstName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLastName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNickName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPasswordExp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTypeText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAccountState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLogonAttempt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteUser = new System.Windows.Forms.PictureBox();
            this.btnAddUser = new System.Windows.Forms.PictureBox();
            this.btnPrintUserCard = new System.Windows.Forms.PictureBox();
            this.tabPageItems = new System.Windows.Forms.TabPage();
            this.listViewArticle = new System.Windows.Forms.ListView();
            this.columnArticleNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBarcode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnStock = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCategory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteArticle = new System.Windows.Forms.PictureBox();
            this.btnAddArticle = new System.Windows.Forms.PictureBox();
            this.tabPageArticleCategory = new System.Windows.Forms.TabPage();
            this.listViewArticleCategory = new System.Windows.Forms.ListView();
            this.columnDescr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDiscount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTax = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteArticleCategory = new System.Windows.Forms.PictureBox();
            this.btnAddArticleCategory = new System.Windows.Forms.PictureBox();
            this.tabPageDiscount = new System.Windows.Forms.TabPage();
            this.listViewDiscount = new System.Windows.Forms.ListView();
            this.columnHeaderDiscount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1Plus1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRabatt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteDiscount = new System.Windows.Forms.PictureBox();
            this.btnAddDiscount = new System.Windows.Forms.PictureBox();
            this.tabPageCoupon = new System.Windows.Forms.TabPage();
            this.listViewCoupon = new System.Windows.Forms.ListView();
            this.columnSpender = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSerialNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnValid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnRedeemed = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDelCoupon = new System.Windows.Forms.PictureBox();
            this.btnAddCoupon = new System.Windows.Forms.PictureBox();
            this.btnPrintCoupon = new System.Windows.Forms.PictureBox();
            this.tabPageReport = new System.Windows.Forms.TabPage();
            this.tabControlReport = new System.Windows.Forms.TabControl();
            this.tabPageSales = new System.Windows.Forms.TabPage();
            this.reportViewerSales = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tabPageArticle = new System.Windows.Forms.TabPage();
            this.reportViewerProducts = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tabPageCoup = new System.Windows.Forms.TabPage();
            this.reportViewerCoupon = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tabPageRabatte = new System.Windows.Forms.TabPage();
            this.reportViewerDiscount = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tabPageProduct = new System.Windows.Forms.TabPage();
            this.reportViewerPriceList = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btnLogOut = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SalesReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CouponReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceListReportBindingSource)).BeginInit();
            this.tabControlAdmin.SuspendLayout();
            this.tabPageUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintUserCard)).BeginInit();
            this.tabPageItems.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteArticle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddArticle)).BeginInit();
            this.tabPageArticleCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteArticleCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddArticleCategory)).BeginInit();
            this.tabPageDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddDiscount)).BeginInit();
            this.tabPageCoupon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelCoupon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCoupon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintCoupon)).BeginInit();
            this.tabPageReport.SuspendLayout();
            this.tabControlReport.SuspendLayout();
            this.tabPageSales.SuspendLayout();
            this.tabPageArticle.SuspendLayout();
            this.tabPageCoup.SuspendLayout();
            this.tabPageRabatte.SuspendLayout();
            this.tabPageProduct.SuspendLayout();
            this.SuspendLayout();
            // 
            // SalesReportBindingSource
            // 
            this.SalesReportBindingSource.DataMember = "Invoices";
            this.SalesReportBindingSource.DataSource = typeof(Knoll.Cashsystem.Reports.SalesReport);
            // 
            // ProductReportBindingSource
            // 
            this.ProductReportBindingSource.DataMember = "Articles";
            this.ProductReportBindingSource.DataSource = typeof(Knoll.Cashsystem.Reports.ProductReport);
            // 
            // CouponReportBindingSource
            // 
            this.CouponReportBindingSource.DataMember = "Coupons";
            this.CouponReportBindingSource.DataSource = typeof(Knoll.Cashsystem.Reports.CouponReport);
            // 
            // DiscountReportBindingSource
            // 
            this.DiscountReportBindingSource.DataMember = "DiscountsGranted";
            this.DiscountReportBindingSource.DataSource = typeof(Knoll.Cashsystem.Reports.DiscountReport);
            // 
            // PriceListReportBindingSource
            // 
            this.PriceListReportBindingSource.DataMember = "Articles";
            this.PriceListReportBindingSource.DataSource = typeof(Knoll.Cashsystem.Reports.PriceListReport);
            // 
            // tabControlAdmin
            // 
            this.tabControlAdmin.Controls.Add(this.tabPageUser);
            this.tabControlAdmin.Controls.Add(this.tabPageItems);
            this.tabControlAdmin.Controls.Add(this.tabPageArticleCategory);
            this.tabControlAdmin.Controls.Add(this.tabPageDiscount);
            this.tabControlAdmin.Controls.Add(this.tabPageCoupon);
            this.tabControlAdmin.Controls.Add(this.tabPageReport);
            this.tabControlAdmin.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlAdmin.ItemSize = new System.Drawing.Size(200, 30);
            this.tabControlAdmin.Location = new System.Drawing.Point(0, 0);
            this.tabControlAdmin.Name = "tabControlAdmin";
            this.tabControlAdmin.SelectedIndex = 0;
            this.tabControlAdmin.Size = new System.Drawing.Size(859, 670);
            this.tabControlAdmin.TabIndex = 0;
            // 
            // tabPageUser
            // 
            this.tabPageUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabPageUser.Controls.Add(this.listViewUser);
            this.tabPageUser.Controls.Add(this.btnDeleteUser);
            this.tabPageUser.Controls.Add(this.btnAddUser);
            this.tabPageUser.Controls.Add(this.btnPrintUserCard);
            this.tabPageUser.Location = new System.Drawing.Point(4, 34);
            this.tabPageUser.Name = "tabPageUser";
            this.tabPageUser.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUser.Size = new System.Drawing.Size(851, 632);
            this.tabPageUser.TabIndex = 0;
            this.tabPageUser.Text = "Benutzer";
            // 
            // listViewUser
            // 
            this.listViewUser.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnPersonalNo,
            this.columnFirstName,
            this.columnLastName,
            this.columnNickName,
            this.columnPasswordExp,
            this.columnTypeText,
            this.columnAccountState,
            this.columnLogonAttempt});
            this.listViewUser.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewUser.Location = new System.Drawing.Point(0, 43);
            this.listViewUser.Name = "listViewUser";
            this.listViewUser.Size = new System.Drawing.Size(852, 589);
            this.listViewUser.TabIndex = 0;
            this.listViewUser.UseCompatibleStateImageBehavior = false;
            this.listViewUser.View = System.Windows.Forms.View.Details;
            this.listViewUser.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewUser_MouseDoubleClick);
            // 
            // columnPersonalNo
            // 
            this.columnPersonalNo.Text = "Personal Nr.";
            this.columnPersonalNo.Width = 92;
            // 
            // columnFirstName
            // 
            this.columnFirstName.Text = "Vorname";
            this.columnFirstName.Width = 110;
            // 
            // columnLastName
            // 
            this.columnLastName.Text = "Nachname";
            this.columnLastName.Width = 114;
            // 
            // columnNickName
            // 
            this.columnNickName.Text = "Benutzername";
            this.columnNickName.Width = 133;
            // 
            // columnPasswordExp
            // 
            this.columnPasswordExp.Text = "Passwort läuft ab";
            this.columnPasswordExp.Width = 118;
            // 
            // columnTypeText
            // 
            this.columnTypeText.Text = "Rolle";
            this.columnTypeText.Width = 70;
            // 
            // columnAccountState
            // 
            this.columnAccountState.Text = "Account Status";
            this.columnAccountState.Width = 107;
            // 
            // columnLogonAttempt
            // 
            this.columnLogonAttempt.Text = "Login Versuche";
            this.columnLogonAttempt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnLogonAttempt.Width = 105;
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteUser.Image = global::Knoll.Cashsystem.Properties.Resources.cross;
            this.btnDeleteUser.ImageLocation = "";
            this.btnDeleteUser.Location = new System.Drawing.Point(46, 6);
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.Size = new System.Drawing.Size(34, 41);
            this.btnDeleteUser.TabIndex = 6;
            this.btnDeleteUser.TabStop = false;
            this.btnDeleteUser.Click += new System.EventHandler(this.btnDeleteUser_Click);
            // 
            // btnAddUser
            // 
            this.btnAddUser.BackColor = System.Drawing.Color.Transparent;
            this.btnAddUser.Image = global::Knoll.Cashsystem.Properties.Resources.add;
            this.btnAddUser.ImageLocation = "";
            this.btnAddUser.Location = new System.Drawing.Point(6, 6);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(34, 41);
            this.btnAddUser.TabIndex = 5;
            this.btnAddUser.TabStop = false;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // btnPrintUserCard
            // 
            this.btnPrintUserCard.BackColor = System.Drawing.Color.Transparent;
            this.btnPrintUserCard.Image = global::Knoll.Cashsystem.Properties.Resources.vcard;
            this.btnPrintUserCard.ImageLocation = "";
            this.btnPrintUserCard.Location = new System.Drawing.Point(86, 9);
            this.btnPrintUserCard.Name = "btnPrintUserCard";
            this.btnPrintUserCard.Size = new System.Drawing.Size(34, 41);
            this.btnPrintUserCard.TabIndex = 7;
            this.btnPrintUserCard.TabStop = false;
            this.btnPrintUserCard.Click += new System.EventHandler(this.btnPrintUserCard_Click);
            // 
            // tabPageItems
            // 
            this.tabPageItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabPageItems.Controls.Add(this.listViewArticle);
            this.tabPageItems.Controls.Add(this.btnDeleteArticle);
            this.tabPageItems.Controls.Add(this.btnAddArticle);
            this.tabPageItems.Location = new System.Drawing.Point(4, 34);
            this.tabPageItems.Name = "tabPageItems";
            this.tabPageItems.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageItems.Size = new System.Drawing.Size(851, 632);
            this.tabPageItems.TabIndex = 1;
            this.tabPageItems.Text = "Produkte";
            // 
            // listViewArticle
            // 
            this.listViewArticle.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnArticleNo,
            this.columnBarcode,
            this.columnDescription,
            this.columnPrice,
            this.columnStock,
            this.columnCategory});
            this.listViewArticle.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewArticle.Location = new System.Drawing.Point(0, 43);
            this.listViewArticle.Name = "listViewArticle";
            this.listViewArticle.Size = new System.Drawing.Size(852, 589);
            this.listViewArticle.TabIndex = 1;
            this.listViewArticle.UseCompatibleStateImageBehavior = false;
            this.listViewArticle.View = System.Windows.Forms.View.Details;
            this.listViewArticle.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewArticle_MouseDoubleClick);
            // 
            // columnArticleNo
            // 
            this.columnArticleNo.Text = "Artikel Nr.";
            this.columnArticleNo.Width = 74;
            // 
            // columnBarcode
            // 
            this.columnBarcode.Text = "Barcode";
            this.columnBarcode.Width = 154;
            // 
            // columnDescription
            // 
            this.columnDescription.Text = "Bezeichnung";
            this.columnDescription.Width = 317;
            // 
            // columnPrice
            // 
            this.columnPrice.DisplayIndex = 4;
            this.columnPrice.Text = "Preis";
            this.columnPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnPrice.Width = 93;
            // 
            // columnStock
            // 
            this.columnStock.DisplayIndex = 5;
            this.columnStock.Text = "Lagerstand";
            this.columnStock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnStock.Width = 80;
            // 
            // columnCategory
            // 
            this.columnCategory.DisplayIndex = 3;
            this.columnCategory.Text = "Produktgruppe";
            this.columnCategory.Width = 131;
            // 
            // btnDeleteArticle
            // 
            this.btnDeleteArticle.Image = global::Knoll.Cashsystem.Properties.Resources.cross;
            this.btnDeleteArticle.ImageLocation = "";
            this.btnDeleteArticle.Location = new System.Drawing.Point(46, 6);
            this.btnDeleteArticle.Name = "btnDeleteArticle";
            this.btnDeleteArticle.Size = new System.Drawing.Size(34, 38);
            this.btnDeleteArticle.TabIndex = 4;
            this.btnDeleteArticle.TabStop = false;
            this.btnDeleteArticle.Click += new System.EventHandler(this.btnDeleteArticle_Click);
            // 
            // btnAddArticle
            // 
            this.btnAddArticle.Image = global::Knoll.Cashsystem.Properties.Resources.add;
            this.btnAddArticle.ImageLocation = "";
            this.btnAddArticle.Location = new System.Drawing.Point(6, 6);
            this.btnAddArticle.Name = "btnAddArticle";
            this.btnAddArticle.Size = new System.Drawing.Size(34, 38);
            this.btnAddArticle.TabIndex = 3;
            this.btnAddArticle.TabStop = false;
            this.btnAddArticle.Click += new System.EventHandler(this.btnAddArticle_Click);
            // 
            // tabPageArticleCategory
            // 
            this.tabPageArticleCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabPageArticleCategory.Controls.Add(this.listViewArticleCategory);
            this.tabPageArticleCategory.Controls.Add(this.btnDeleteArticleCategory);
            this.tabPageArticleCategory.Controls.Add(this.btnAddArticleCategory);
            this.tabPageArticleCategory.Location = new System.Drawing.Point(4, 34);
            this.tabPageArticleCategory.Name = "tabPageArticleCategory";
            this.tabPageArticleCategory.Size = new System.Drawing.Size(851, 632);
            this.tabPageArticleCategory.TabIndex = 2;
            this.tabPageArticleCategory.Text = "Produktgruppen";
            // 
            // listViewArticleCategory
            // 
            this.listViewArticleCategory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnDescr,
            this.columnDiscount,
            this.columnTax});
            this.listViewArticleCategory.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewArticleCategory.Location = new System.Drawing.Point(0, 42);
            this.listViewArticleCategory.Name = "listViewArticleCategory";
            this.listViewArticleCategory.Size = new System.Drawing.Size(852, 589);
            this.listViewArticleCategory.TabIndex = 7;
            this.listViewArticleCategory.UseCompatibleStateImageBehavior = false;
            this.listViewArticleCategory.View = System.Windows.Forms.View.Details;
            this.listViewArticleCategory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewArticleCategory_MouseDoubleClick);
            // 
            // columnDescr
            // 
            this.columnDescr.Text = "Bezeichnung";
            this.columnDescr.Width = 539;
            // 
            // columnDiscount
            // 
            this.columnDiscount.Text = "Rabatt";
            this.columnDiscount.Width = 212;
            // 
            // columnTax
            // 
            this.columnTax.Text = "Steuergruppe";
            this.columnTax.Width = 98;
            // 
            // btnDeleteArticleCategory
            // 
            this.btnDeleteArticleCategory.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteArticleCategory.Image = global::Knoll.Cashsystem.Properties.Resources.cross;
            this.btnDeleteArticleCategory.ImageLocation = "";
            this.btnDeleteArticleCategory.Location = new System.Drawing.Point(46, 6);
            this.btnDeleteArticleCategory.Name = "btnDeleteArticleCategory";
            this.btnDeleteArticleCategory.Size = new System.Drawing.Size(34, 41);
            this.btnDeleteArticleCategory.TabIndex = 9;
            this.btnDeleteArticleCategory.TabStop = false;
            this.btnDeleteArticleCategory.Click += new System.EventHandler(this.btnDeleteArticleCategory_Click);
            // 
            // btnAddArticleCategory
            // 
            this.btnAddArticleCategory.BackColor = System.Drawing.Color.Transparent;
            this.btnAddArticleCategory.Image = global::Knoll.Cashsystem.Properties.Resources.add;
            this.btnAddArticleCategory.ImageLocation = "";
            this.btnAddArticleCategory.Location = new System.Drawing.Point(6, 6);
            this.btnAddArticleCategory.Name = "btnAddArticleCategory";
            this.btnAddArticleCategory.Size = new System.Drawing.Size(34, 41);
            this.btnAddArticleCategory.TabIndex = 8;
            this.btnAddArticleCategory.TabStop = false;
            this.btnAddArticleCategory.Click += new System.EventHandler(this.btnAddArticleCategory_Click);
            // 
            // tabPageDiscount
            // 
            this.tabPageDiscount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabPageDiscount.Controls.Add(this.listViewDiscount);
            this.tabPageDiscount.Controls.Add(this.btnDeleteDiscount);
            this.tabPageDiscount.Controls.Add(this.btnAddDiscount);
            this.tabPageDiscount.Location = new System.Drawing.Point(4, 34);
            this.tabPageDiscount.Name = "tabPageDiscount";
            this.tabPageDiscount.Size = new System.Drawing.Size(851, 632);
            this.tabPageDiscount.TabIndex = 4;
            this.tabPageDiscount.Text = "Rabatte";
            // 
            // listViewDiscount
            // 
            this.listViewDiscount.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderDiscount,
            this.columnHeader1Plus1,
            this.columnHeaderRabatt});
            this.listViewDiscount.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewDiscount.Location = new System.Drawing.Point(0, 42);
            this.listViewDiscount.Name = "listViewDiscount";
            this.listViewDiscount.Size = new System.Drawing.Size(852, 589);
            this.listViewDiscount.TabIndex = 10;
            this.listViewDiscount.UseCompatibleStateImageBehavior = false;
            this.listViewDiscount.View = System.Windows.Forms.View.Details;
            this.listViewDiscount.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewDiscount_MouseDoubleClick);
            // 
            // columnHeaderDiscount
            // 
            this.columnHeaderDiscount.Text = "Rabatt";
            this.columnHeaderDiscount.Width = 661;
            // 
            // columnHeader1Plus1
            // 
            this.columnHeader1Plus1.Text = "1 + 1 ";
            this.columnHeader1Plus1.Width = 83;
            // 
            // columnHeaderRabatt
            // 
            this.columnHeaderRabatt.Text = "Prozent";
            this.columnHeaderRabatt.Width = 104;
            // 
            // btnDeleteDiscount
            // 
            this.btnDeleteDiscount.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteDiscount.Image = global::Knoll.Cashsystem.Properties.Resources.cross;
            this.btnDeleteDiscount.ImageLocation = "";
            this.btnDeleteDiscount.Location = new System.Drawing.Point(46, 6);
            this.btnDeleteDiscount.Name = "btnDeleteDiscount";
            this.btnDeleteDiscount.Size = new System.Drawing.Size(34, 41);
            this.btnDeleteDiscount.TabIndex = 12;
            this.btnDeleteDiscount.TabStop = false;
            this.btnDeleteDiscount.Click += new System.EventHandler(this.btnDeleteDiscount_Click);
            // 
            // btnAddDiscount
            // 
            this.btnAddDiscount.BackColor = System.Drawing.Color.Transparent;
            this.btnAddDiscount.Image = global::Knoll.Cashsystem.Properties.Resources.add;
            this.btnAddDiscount.ImageLocation = "";
            this.btnAddDiscount.Location = new System.Drawing.Point(6, 6);
            this.btnAddDiscount.Name = "btnAddDiscount";
            this.btnAddDiscount.Size = new System.Drawing.Size(34, 41);
            this.btnAddDiscount.TabIndex = 11;
            this.btnAddDiscount.TabStop = false;
            this.btnAddDiscount.Click += new System.EventHandler(this.btnAddDiscount_Click);
            // 
            // tabPageCoupon
            // 
            this.tabPageCoupon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabPageCoupon.Controls.Add(this.listViewCoupon);
            this.tabPageCoupon.Controls.Add(this.btnDelCoupon);
            this.tabPageCoupon.Controls.Add(this.btnAddCoupon);
            this.tabPageCoupon.Controls.Add(this.btnPrintCoupon);
            this.tabPageCoupon.Location = new System.Drawing.Point(4, 34);
            this.tabPageCoupon.Name = "tabPageCoupon";
            this.tabPageCoupon.Size = new System.Drawing.Size(851, 632);
            this.tabPageCoupon.TabIndex = 5;
            this.tabPageCoupon.Text = "Gutscheine";
            // 
            // listViewCoupon
            // 
            this.listViewCoupon.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnSpender,
            this.columnSerialNo,
            this.columnValue,
            this.columnValid,
            this.columnRedeemed});
            this.listViewCoupon.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewCoupon.Location = new System.Drawing.Point(-1, 40);
            this.listViewCoupon.Name = "listViewCoupon";
            this.listViewCoupon.Size = new System.Drawing.Size(852, 589);
            this.listViewCoupon.TabIndex = 10;
            this.listViewCoupon.UseCompatibleStateImageBehavior = false;
            this.listViewCoupon.View = System.Windows.Forms.View.Details;
            this.listViewCoupon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewCoupon_MouseDoubleClick);
            // 
            // columnSpender
            // 
            this.columnSpender.Text = "Ausgeber";
            this.columnSpender.Width = 241;
            // 
            // columnSerialNo
            // 
            this.columnSerialNo.Text = "Seriennummer";
            this.columnSerialNo.Width = 263;
            // 
            // columnValue
            // 
            this.columnValue.Text = "Wert";
            this.columnValue.Width = 93;
            // 
            // columnValid
            // 
            this.columnValid.Text = "Gültig";
            this.columnValid.Width = 83;
            // 
            // columnRedeemed
            // 
            this.columnRedeemed.Text = "Eingelöst";
            this.columnRedeemed.Width = 168;
            // 
            // btnDelCoupon
            // 
            this.btnDelCoupon.BackColor = System.Drawing.Color.Transparent;
            this.btnDelCoupon.Image = global::Knoll.Cashsystem.Properties.Resources.cross;
            this.btnDelCoupon.ImageLocation = "";
            this.btnDelCoupon.Location = new System.Drawing.Point(45, 4);
            this.btnDelCoupon.Name = "btnDelCoupon";
            this.btnDelCoupon.Size = new System.Drawing.Size(34, 41);
            this.btnDelCoupon.TabIndex = 12;
            this.btnDelCoupon.TabStop = false;
            this.btnDelCoupon.Click += new System.EventHandler(this.btnDelCoupon_Click);
            // 
            // btnAddCoupon
            // 
            this.btnAddCoupon.BackColor = System.Drawing.Color.Transparent;
            this.btnAddCoupon.Image = global::Knoll.Cashsystem.Properties.Resources.add;
            this.btnAddCoupon.ImageLocation = "";
            this.btnAddCoupon.Location = new System.Drawing.Point(5, 4);
            this.btnAddCoupon.Name = "btnAddCoupon";
            this.btnAddCoupon.Size = new System.Drawing.Size(34, 41);
            this.btnAddCoupon.TabIndex = 11;
            this.btnAddCoupon.TabStop = false;
            this.btnAddCoupon.Click += new System.EventHandler(this.btnAddCoupon_Click);
            // 
            // btnPrintCoupon
            // 
            this.btnPrintCoupon.BackColor = System.Drawing.Color.Transparent;
            this.btnPrintCoupon.Image = global::Knoll.Cashsystem.Properties.Resources.printer;
            this.btnPrintCoupon.ImageLocation = "";
            this.btnPrintCoupon.Location = new System.Drawing.Point(85, 6);
            this.btnPrintCoupon.Name = "btnPrintCoupon";
            this.btnPrintCoupon.Size = new System.Drawing.Size(34, 41);
            this.btnPrintCoupon.TabIndex = 13;
            this.btnPrintCoupon.TabStop = false;
            this.btnPrintCoupon.Click += new System.EventHandler(this.btnPrintCoupon_Click);
            // 
            // tabPageReport
            // 
            this.tabPageReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.tabPageReport.Controls.Add(this.tabControlReport);
            this.tabPageReport.Location = new System.Drawing.Point(4, 34);
            this.tabPageReport.Name = "tabPageReport";
            this.tabPageReport.Size = new System.Drawing.Size(851, 632);
            this.tabPageReport.TabIndex = 3;
            this.tabPageReport.Text = "Berichte";
            // 
            // tabControlReport
            // 
            this.tabControlReport.Controls.Add(this.tabPageSales);
            this.tabControlReport.Controls.Add(this.tabPageArticle);
            this.tabControlReport.Controls.Add(this.tabPageCoup);
            this.tabControlReport.Controls.Add(this.tabPageRabatte);
            this.tabControlReport.Controls.Add(this.tabPageProduct);
            this.tabControlReport.Location = new System.Drawing.Point(0, 0);
            this.tabControlReport.Name = "tabControlReport";
            this.tabControlReport.SelectedIndex = 0;
            this.tabControlReport.Size = new System.Drawing.Size(855, 636);
            this.tabControlReport.TabIndex = 0;
            // 
            // tabPageSales
            // 
            this.tabPageSales.Controls.Add(this.reportViewerSales);
            this.tabPageSales.Location = new System.Drawing.Point(4, 28);
            this.tabPageSales.Name = "tabPageSales";
            this.tabPageSales.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSales.Size = new System.Drawing.Size(847, 604);
            this.tabPageSales.TabIndex = 0;
            this.tabPageSales.Text = "Umsatz";
            this.tabPageSales.UseVisualStyleBackColor = true;
            // 
            // reportViewerSales
            // 
            reportDataSource1.Name = "InvoiceDataSet";
            reportDataSource1.Value = this.SalesReportBindingSource;
            this.reportViewerSales.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewerSales.LocalReport.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.Sales.rdlc";
            this.reportViewerSales.Location = new System.Drawing.Point(0, 0);
            this.reportViewerSales.Name = "reportViewerSales";
            this.reportViewerSales.ServerReport.BearerToken = null;
            this.reportViewerSales.Size = new System.Drawing.Size(844, 604);
            this.reportViewerSales.TabIndex = 0;
            this.reportViewerSales.Load += new System.EventHandler(this.reportViewerSales_Load);
            // 
            // tabPageArticle
            // 
            this.tabPageArticle.Controls.Add(this.reportViewerProducts);
            this.tabPageArticle.Location = new System.Drawing.Point(4, 28);
            this.tabPageArticle.Name = "tabPageArticle";
            this.tabPageArticle.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageArticle.Size = new System.Drawing.Size(847, 604);
            this.tabPageArticle.TabIndex = 1;
            this.tabPageArticle.Text = "Lagernde Produkte";
            this.tabPageArticle.UseVisualStyleBackColor = true;
            // 
            // reportViewerProducts
            // 
            reportDataSource2.Name = "ProductsDataSet";
            reportDataSource2.Value = this.ProductReportBindingSource;
            this.reportViewerProducts.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewerProducts.LocalReport.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.Product.rdlc";
            this.reportViewerProducts.Location = new System.Drawing.Point(0, 0);
            this.reportViewerProducts.Name = "reportViewerProducts";
            this.reportViewerProducts.ServerReport.BearerToken = null;
            this.reportViewerProducts.Size = new System.Drawing.Size(847, 604);
            this.reportViewerProducts.TabIndex = 0;
            this.reportViewerProducts.Load += new System.EventHandler(this.reportViewerProducts_Load);
            // 
            // tabPageCoup
            // 
            this.tabPageCoup.Controls.Add(this.reportViewerCoupon);
            this.tabPageCoup.Location = new System.Drawing.Point(4, 28);
            this.tabPageCoup.Name = "tabPageCoup";
            this.tabPageCoup.Size = new System.Drawing.Size(847, 604);
            this.tabPageCoup.TabIndex = 2;
            this.tabPageCoup.Text = "Gutscheine";
            this.tabPageCoup.UseVisualStyleBackColor = true;
            // 
            // reportViewerCoupon
            // 
            reportDataSource3.Name = "CouponDataSet";
            reportDataSource3.Value = this.CouponReportBindingSource;
            this.reportViewerCoupon.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewerCoupon.LocalReport.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.Coupon.rdlc";
            this.reportViewerCoupon.Location = new System.Drawing.Point(0, 0);
            this.reportViewerCoupon.Name = "reportViewerCoupon";
            this.reportViewerCoupon.ServerReport.BearerToken = null;
            this.reportViewerCoupon.Size = new System.Drawing.Size(847, 604);
            this.reportViewerCoupon.TabIndex = 0;
            this.reportViewerCoupon.Load += new System.EventHandler(this.reportViewerCoupon_Load);
            // 
            // tabPageRabatte
            // 
            this.tabPageRabatte.Controls.Add(this.reportViewerDiscount);
            this.tabPageRabatte.Location = new System.Drawing.Point(4, 28);
            this.tabPageRabatte.Name = "tabPageRabatte";
            this.tabPageRabatte.Size = new System.Drawing.Size(847, 604);
            this.tabPageRabatte.TabIndex = 3;
            this.tabPageRabatte.Text = "Rabatte";
            this.tabPageRabatte.UseVisualStyleBackColor = true;
            // 
            // reportViewerDiscount
            // 
            reportDataSource4.Name = "DiscountGrantedDataSet";
            reportDataSource4.Value = this.DiscountReportBindingSource;
            this.reportViewerDiscount.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewerDiscount.LocalReport.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.Discount.rdlc";
            this.reportViewerDiscount.Location = new System.Drawing.Point(0, 0);
            this.reportViewerDiscount.Name = "reportViewerDiscount";
            this.reportViewerDiscount.ServerReport.BearerToken = null;
            this.reportViewerDiscount.Size = new System.Drawing.Size(847, 604);
            this.reportViewerDiscount.TabIndex = 0;
            this.reportViewerDiscount.Load += new System.EventHandler(this.reportViewerDiscount_Load);
            // 
            // tabPageProduct
            // 
            this.tabPageProduct.Controls.Add(this.reportViewerPriceList);
            this.tabPageProduct.Location = new System.Drawing.Point(4, 28);
            this.tabPageProduct.Name = "tabPageProduct";
            this.tabPageProduct.Size = new System.Drawing.Size(847, 604);
            this.tabPageProduct.TabIndex = 4;
            this.tabPageProduct.Text = "Preisliste";
            this.tabPageProduct.UseVisualStyleBackColor = true;
            // 
            // reportViewerPriceList
            // 
            reportDataSource5.Name = "PriceListDataSet";
            reportDataSource5.Value = this.PriceListReportBindingSource;
            this.reportViewerPriceList.LocalReport.DataSources.Add(reportDataSource5);
            this.reportViewerPriceList.LocalReport.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.ProductPriceList.rdlc";
            this.reportViewerPriceList.Location = new System.Drawing.Point(0, 0);
            this.reportViewerPriceList.Name = "reportViewerPriceList";
            this.reportViewerPriceList.ServerReport.BearerToken = null;
            this.reportViewerPriceList.Size = new System.Drawing.Size(847, 604);
            this.reportViewerPriceList.TabIndex = 0;
            this.reportViewerPriceList.Load += new System.EventHandler(this.reportViewerPriceList_Load);
            // 
            // btnLogOut
            // 
            this.btnLogOut.Location = new System.Drawing.Point(775, 4);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(75, 23);
            this.btnLogOut.TabIndex = 1;
            this.btnLogOut.Text = "Abmelden";
            this.btnLogOut.UseVisualStyleBackColor = true;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(858, 670);
            this.Controls.Add(this.btnLogOut);
            this.Controls.Add(this.tabControlAdmin);
            this.Name = "AdminForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Admin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminForm_FormClosed);
            this.Load += new System.EventHandler(this.Admin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SalesReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CouponReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceListReportBindingSource)).EndInit();
            this.tabControlAdmin.ResumeLayout(false);
            this.tabPageUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintUserCard)).EndInit();
            this.tabPageItems.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteArticle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddArticle)).EndInit();
            this.tabPageArticleCategory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteArticleCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddArticleCategory)).EndInit();
            this.tabPageDiscount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDeleteDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddDiscount)).EndInit();
            this.tabPageCoupon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDelCoupon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddCoupon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrintCoupon)).EndInit();
            this.tabPageReport.ResumeLayout(false);
            this.tabControlReport.ResumeLayout(false);
            this.tabPageSales.ResumeLayout(false);
            this.tabPageArticle.ResumeLayout(false);
            this.tabPageCoup.ResumeLayout(false);
            this.tabPageRabatte.ResumeLayout(false);
            this.tabPageProduct.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlAdmin;
        private System.Windows.Forms.TabPage tabPageUser;
        private System.Windows.Forms.TabPage tabPageItems;
        private System.Windows.Forms.ListView listViewUser;
        private System.Windows.Forms.ColumnHeader columnPersonalNo;
        private System.Windows.Forms.ColumnHeader columnFirstName;
        private System.Windows.Forms.ColumnHeader columnLastName;
        private System.Windows.Forms.ColumnHeader columnNickName;
        private System.Windows.Forms.ColumnHeader columnPasswordExp;
        private System.Windows.Forms.ColumnHeader columnTypeText;
        private System.Windows.Forms.ColumnHeader columnAccountState;
        private System.Windows.Forms.ColumnHeader columnLogonAttempt;
        private System.Windows.Forms.ListView listViewArticle;
        private System.Windows.Forms.ColumnHeader columnDescription;
        private System.Windows.Forms.ColumnHeader columnArticleNo;
        private System.Windows.Forms.ColumnHeader columnPrice;
        private System.Windows.Forms.ColumnHeader columnStock;
        private System.Windows.Forms.PictureBox btnAddArticle;
        private System.Windows.Forms.PictureBox btnDeleteArticle;
        private System.Windows.Forms.PictureBox btnDeleteUser;
        private System.Windows.Forms.PictureBox btnAddUser;
        private System.Windows.Forms.TabPage tabPageArticleCategory;
        private System.Windows.Forms.TabPage tabPageReport;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.ListView listViewArticleCategory;
        private System.Windows.Forms.ColumnHeader columnDescr;
        private System.Windows.Forms.PictureBox btnDeleteArticleCategory;
        private System.Windows.Forms.PictureBox btnAddArticleCategory;
        private System.Windows.Forms.ColumnHeader columnBarcode;
        private System.Windows.Forms.ColumnHeader columnCategory;
        private System.Windows.Forms.ColumnHeader columnDiscount;
        private System.Windows.Forms.TabPage tabPageDiscount;
        private System.Windows.Forms.TabPage tabPageCoupon;
        private System.Windows.Forms.ListView listViewCoupon;
        private System.Windows.Forms.ColumnHeader columnSpender;
        private System.Windows.Forms.ColumnHeader columnSerialNo;
        private System.Windows.Forms.ColumnHeader columnValue;
        private System.Windows.Forms.ColumnHeader columnValid;
        private System.Windows.Forms.ColumnHeader columnRedeemed;
        private System.Windows.Forms.PictureBox btnDelCoupon;
        private System.Windows.Forms.PictureBox btnAddCoupon;
        private System.Windows.Forms.TabControl tabControlReport;
        private System.Windows.Forms.TabPage tabPageSales;
        private System.Windows.Forms.TabPage tabPageArticle;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewerSales;
        private System.Windows.Forms.BindingSource SalesReportBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewerProducts;
        private System.Windows.Forms.BindingSource ProductReportBindingSource;
        private System.Windows.Forms.TabPage tabPageCoup;
        private System.Windows.Forms.TabPage tabPageRabatte;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewerCoupon;
        private System.Windows.Forms.TabPage tabPageProduct;
        private System.Windows.Forms.BindingSource CouponReportBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewerPriceList;
        private System.Windows.Forms.BindingSource PriceListReportBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewerDiscount;
        private System.Windows.Forms.BindingSource DiscountReportBindingSource;
        private System.Windows.Forms.ListView listViewDiscount;
        private System.Windows.Forms.ColumnHeader columnHeaderDiscount;
        private System.Windows.Forms.ColumnHeader columnHeader1Plus1;
        private System.Windows.Forms.ColumnHeader columnHeaderRabatt;
        private System.Windows.Forms.PictureBox btnDeleteDiscount;
        private System.Windows.Forms.PictureBox btnAddDiscount;
        private System.Windows.Forms.ColumnHeader columnTax;
        private System.Windows.Forms.PictureBox btnPrintUserCard;
        private System.Windows.Forms.PictureBox btnPrintCoupon;
    }
}
