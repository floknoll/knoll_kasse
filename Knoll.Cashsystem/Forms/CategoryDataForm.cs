﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class CategoryDataForm : Form
    {
        private ArticleCategory category = null;

        public CategoryDataForm()
        {
            InitializeComponent();
        }

        public CategoryDataForm(ArticleCategory category)
        {
            InitializeComponent();
            this.category = category;
        }

        private void CategoryDataForm_Load(object sender, EventArgs e)
        {
            List<Discount> discounts = Discount.GetList();
            this.cmbDiscount.DataSource = discounts;
            cmbDiscount.DisplayMember = "Description";
            cmbDiscount.ValueMember = "DiscountId";
            this.cmbTax.Items.Add("A");
            this.cmbTax.Items.Add("B");
            if (this.category != null)
            {
                this.txtDescription.Text = this.category.Description;
                this.cmbDiscount.SelectedValue = this.category.DiscountId;
                this.cmbTax.SelectedText = this.category.Tax;
            }

            this.txtDescription.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (SaveArticleCategory())
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show("Kein Datensatz betroffen!");
        }
        
        private bool SaveArticleCategory()
        {
            if (this.category == null)
            {
                this.category = new ArticleCategory();
            }
            this.category.Description = this.txtDescription.Text;
            this.category.DiscountId = (decimal)this.cmbDiscount.SelectedValue;
            this.category.Tax = this.cmbTax.Text;

            return this.category.Save();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
