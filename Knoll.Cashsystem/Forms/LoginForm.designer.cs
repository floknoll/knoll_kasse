﻿namespace Knoll.Cashsystem
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblNewPassword = new System.Windows.Forms.Label();
            this.lblRepeatPassword = new System.Windows.Forms.Label();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.txtRepeatPassword = new System.Windows.Forms.TextBox();
            this.printDocumentPersonCard = new System.Drawing.Printing.PrintDocument();
            this.printDialogPersonCard = new System.Windows.Forms.PrintDialog();
            this.btnInputHelp = new System.Windows.Forms.Button();
            this.lblSecurityQuestion1 = new System.Windows.Forms.Label();
            this.lblSecurityQuestion2 = new System.Windows.Forms.Label();
            this.txtSecurityQuestion2 = new System.Windows.Forms.TextBox();
            this.txtSecurityQuestion1 = new System.Windows.Forms.TextBox();
            this.lblSecurityQuestion = new System.Windows.Forms.Label();
            this.btnForgotPW = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLogin.Location = new System.Drawing.Point(15, 435);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(121, 23);
            this.btnLogin.TabIndex = 9;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(154, 435);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(121, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUser.Location = new System.Drawing.Point(12, 107);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(82, 14);
            this.lblUser.TabIndex = 2;
            this.lblUser.Text = "Benutzername:";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(14, 147);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(55, 14);
            this.lblPassword.TabIndex = 3;
            this.lblPassword.Text = "Passwort:";
            // 
            // txtUser
            // 
            this.txtUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUser.Location = new System.Drawing.Point(15, 124);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(260, 20);
            this.txtUser.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(15, 164);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(260, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // lblNewPassword
            // 
            this.lblNewPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNewPassword.AutoSize = true;
            this.lblNewPassword.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNewPassword.Location = new System.Drawing.Point(12, 243);
            this.lblNewPassword.Name = "lblNewPassword";
            this.lblNewPassword.Size = new System.Drawing.Size(89, 14);
            this.lblNewPassword.TabIndex = 8;
            this.lblNewPassword.Text = "Neues Passwort:";
            this.lblNewPassword.Visible = false;
            // 
            // lblRepeatPassword
            // 
            this.lblRepeatPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRepeatPassword.AutoSize = true;
            this.lblRepeatPassword.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepeatPassword.Location = new System.Drawing.Point(14, 283);
            this.lblRepeatPassword.Name = "lblRepeatPassword";
            this.lblRepeatPassword.Size = new System.Drawing.Size(119, 14);
            this.lblRepeatPassword.TabIndex = 9;
            this.lblRepeatPassword.Text = "Passwort wiederholen:";
            this.lblRepeatPassword.Visible = false;
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtNewPassword.Location = new System.Drawing.Point(15, 260);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(260, 20);
            this.txtNewPassword.TabIndex = 5;
            this.txtNewPassword.Visible = false;
            // 
            // txtRepeatPassword
            // 
            this.txtRepeatPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtRepeatPassword.Location = new System.Drawing.Point(15, 300);
            this.txtRepeatPassword.Name = "txtRepeatPassword";
            this.txtRepeatPassword.PasswordChar = '*';
            this.txtRepeatPassword.Size = new System.Drawing.Size(260, 20);
            this.txtRepeatPassword.TabIndex = 6;
            this.txtRepeatPassword.Visible = false;
            // 
            // printDialogPersonCard
            // 
            this.printDialogPersonCard.UseEXDialog = true;
            // 
            // btnInputHelp
            // 
            this.btnInputHelp.Location = new System.Drawing.Point(154, 203);
            this.btnInputHelp.Name = "btnInputHelp";
            this.btnInputHelp.Size = new System.Drawing.Size(121, 23);
            this.btnInputHelp.TabIndex = 4;
            this.btnInputHelp.Text = "Eingabehilfe starten";
            this.btnInputHelp.UseVisualStyleBackColor = true;
            this.btnInputHelp.Click += new System.EventHandler(this.btnInputHelp_Click);
            // 
            // lblSecurityQuestion1
            // 
            this.lblSecurityQuestion1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSecurityQuestion1.AutoSize = true;
            this.lblSecurityQuestion1.Location = new System.Drawing.Point(12, 388);
            this.lblSecurityQuestion1.Name = "lblSecurityQuestion1";
            this.lblSecurityQuestion1.Size = new System.Drawing.Size(82, 13);
            this.lblSecurityQuestion1.TabIndex = 13;
            this.lblSecurityQuestion1.Text = "Ihr Geburtsjahr?";
            this.lblSecurityQuestion1.Visible = false;
            // 
            // lblSecurityQuestion2
            // 
            this.lblSecurityQuestion2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSecurityQuestion2.AutoSize = true;
            this.lblSecurityQuestion2.Location = new System.Drawing.Point(14, 349);
            this.lblSecurityQuestion2.Name = "lblSecurityQuestion2";
            this.lblSecurityQuestion2.Size = new System.Drawing.Size(152, 13);
            this.lblSecurityQuestion2.TabIndex = 14;
            this.lblSecurityQuestion2.Text = "Name Ihres ersten Haustieres?";
            this.lblSecurityQuestion2.Visible = false;
            // 
            // txtSecurityQuestion2
            // 
            this.txtSecurityQuestion2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecurityQuestion2.Location = new System.Drawing.Point(15, 404);
            this.txtSecurityQuestion2.Name = "txtSecurityQuestion2";
            this.txtSecurityQuestion2.Size = new System.Drawing.Size(260, 20);
            this.txtSecurityQuestion2.TabIndex = 8;
            this.txtSecurityQuestion2.Visible = false;
            // 
            // txtSecurityQuestion1
            // 
            this.txtSecurityQuestion1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSecurityQuestion1.Location = new System.Drawing.Point(15, 365);
            this.txtSecurityQuestion1.Name = "txtSecurityQuestion1";
            this.txtSecurityQuestion1.Size = new System.Drawing.Size(260, 20);
            this.txtSecurityQuestion1.TabIndex = 7;
            this.txtSecurityQuestion1.Visible = false;
            // 
            // lblSecurityQuestion
            // 
            this.lblSecurityQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSecurityQuestion.AutoSize = true;
            this.lblSecurityQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecurityQuestion.Location = new System.Drawing.Point(14, 333);
            this.lblSecurityQuestion.Name = "lblSecurityQuestion";
            this.lblSecurityQuestion.Size = new System.Drawing.Size(110, 13);
            this.lblSecurityQuestion.TabIndex = 17;
            this.lblSecurityQuestion.Text = "Sicherheitsfragen:";
            this.lblSecurityQuestion.Visible = false;
            // 
            // btnForgotPW
            // 
            this.btnForgotPW.Location = new System.Drawing.Point(15, 203);
            this.btnForgotPW.Name = "btnForgotPW";
            this.btnForgotPW.Size = new System.Drawing.Size(121, 23);
            this.btnForgotPW.TabIndex = 3;
            this.btnForgotPW.Text = "Passwort vergessen";
            this.btnForgotPW.UseVisualStyleBackColor = true;
            this.btnForgotPW.Click += new System.EventHandler(this.btnForgotPW_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Knoll.Cashsystem.Properties.Resources.Knoll_Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(287, 99);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(287, 463);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnForgotPW);
            this.Controls.Add(this.lblSecurityQuestion);
            this.Controls.Add(this.txtSecurityQuestion1);
            this.Controls.Add(this.txtSecurityQuestion2);
            this.Controls.Add(this.lblSecurityQuestion2);
            this.Controls.Add(this.lblSecurityQuestion1);
            this.Controls.Add(this.btnInputHelp);
            this.Controls.Add(this.txtRepeatPassword);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.lblRepeatPassword);
            this.Controls.Add(this.lblNewPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(16, 224);
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
            this.Load += new System.EventHandler(this.LoginForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.Label lblRepeatPassword;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.TextBox txtRepeatPassword;
        private System.Drawing.Printing.PrintDocument printDocumentPersonCard;
        private System.Windows.Forms.PrintDialog printDialogPersonCard;
        private System.Windows.Forms.Button btnInputHelp;
        private System.Windows.Forms.Label lblSecurityQuestion1;
        private System.Windows.Forms.Label lblSecurityQuestion2;
        private System.Windows.Forms.TextBox txtSecurityQuestion2;
        private System.Windows.Forms.TextBox txtSecurityQuestion1;
        private System.Windows.Forms.Label lblSecurityQuestion;
        private System.Windows.Forms.Button btnForgotPW;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}