﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
	public class KnollDataObject
	{
		protected Dictionary<string, object> properties = null;
		protected KnollDataObject parent = null;


		protected static List<T> GetList<T>(string selectCmd)
		{
			KnollConnection.GetInstance().Connection.Open();

			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.CommandText = selectCmd;

			NpgsqlDataReader reader = command.ExecuteReader();

			List<T> rows = new List<T>();
			Dictionary<string, object> row = null;
			while (reader.Read())
			{
				row = new Dictionary<string, object>();
				for (int idx = 0; idx < reader.FieldCount; idx++)
				{
					row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
				}
				rows.Add((T)Activator.CreateInstance(typeof(T), row));
			}
			reader.Close();
			KnollConnection.GetInstance().Connection.Close();
			if (KnollConnection.GetInstance().Connection.State == System.Data.ConnectionState.Open)
				Debug.WriteLine("offen!!!!");
			return rows;
		}

		protected static List<T> GetList<T>(KnollDataObject parent, string selectCmd, params KeyValuePair<string, object>[] parameters)
		{
			KnollConnection.GetInstance().Connection.Open();

			NpgsqlCommand command = new NpgsqlCommand();
			command.Connection = KnollConnection.GetInstance().Connection;
			command.CommandText = selectCmd;
			foreach (KeyValuePair<string, object> kvp in parameters)
			{
				command.Parameters.AddWithValue(kvp.Key, kvp.Value);
			}
			NpgsqlDataReader reader = command.ExecuteReader();

			List<T> rows = new List<T>();
			Dictionary<string, object> row = null;
			while (reader.Read())
			{
				row = new Dictionary<string, object>();
				for (int idx = 0; idx < reader.FieldCount; idx++)
				{
					row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
				}
				rows.Add((T)Activator.CreateInstance(typeof(T), parent, row));
			}
			reader.Close();
			KnollConnection.GetInstance().Connection.Close();
			if (KnollConnection.GetInstance().Connection.State == System.Data.ConnectionState.Open)
				Debug.WriteLine("offen!!!!");
			return rows;
		}





		public KnollDataObject()
		{
			this.properties = new Dictionary<string, object>();
		}
		public KnollDataObject(Dictionary<string, object> row)
		{
			this.properties = row;
		}

		public KnollDataObject(KnollDataObject parent)
		{
			this.properties = new Dictionary<string, object>();
			this.parent = parent;
		}
		public KnollDataObject(KnollDataObject parent, Dictionary<string, object> row)
		{
			this.properties = row;
			this.parent = parent;
		}
		protected void SetValue(string name, object content)
		{
			if (this.properties.ContainsKey(name)) this.properties[name] = content;
			else this.properties.Add(name, content);
		}

		protected T GetValue<T>(string name)
		{
			if (this.properties.ContainsKey(name))
			{
				if (this.properties[name] == null) return (T)default(T);
				else return (T)this.properties[name];
			}
			else return (T)default(T);


		}

	}
}
