﻿using Knoll.BarcodeListener;
using Knoll.Cashsystem.Classes;
using Knoll.Cashsystem.Properties;
using Knoll.Cashsystem.Reports;
using Knoll.Data.Cashsystem;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Knoll.Cashsystem
{
    public partial class LoginForm : Form
    {
        int top = 0;
        BcListener listener = null;
        Process keyboard = null;
        Person person = null;
        Login login = new Login();
        bool fake = false;

        public void SetVisible() { this.Top = top; this.txtPassword.Text = string.Empty; listener.OnLoginDataReceived += Listener_OnLoginDataReceived; }
        public void SetInVisible() { top = this.Top; this.Top = 32000; }

        public LoginForm(bool fake = false)
        {
            InitializeComponent();
            if (!fake)
            {
                listener = BcListener.GetBarcodeListener();
                listener.OnLoginDataReceived += Listener_OnLoginDataReceived;
            }
            this.fake = fake;
        }

        private void Listener_OnLoginDataReceived(object sender, EventArgs e)
        {
            listener.OnLoginDataReceived -= Listener_OnLoginDataReceived;
            var loginDaten = e as LoginEventArgs;
            Invoke(new Action(() => {
                Person person = Person.Get(loginDaten.UserName);
                this.txtUser.Text = loginDaten.UserName;
                this.txtPassword.Text = loginDaten.Password;
                //this.DialogResult = DialogResult.OK;
                //this.Close();
                this.DoLogin(this, person);
                
            }));
        }

        

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (keyboard != null)
                {
                    if (keyboard.ProcessName == "osk")
                    {
                        keyboard.Kill();
                    }
                }
            }
            catch { }
            
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            
            if (ForgotPassword)
            {                
                Person person = Person.Get(this.Username);                
                if (person.SecurityQuestion1 == this.SecurityQuestion1 && person.SecurityQuestion2 == this.SecurityQuestion2)
                {
                    person.Password = Crypt.HashSHAPassword(this.txtNewPassword.Text);
                    person.AccountState = AccountStateEnum.Offen;
                    person.PasswordExpiration = DateTime.Now.AddYears(2);
                    person.LogonAttempt = 0;
                    AccountLog accountLog = person.AccountLogAdd(DateTime.Now, false);
                    accountLog.Message = "Passwort erfolgreich geändert.";
                    person.Save();
                    if (this.txtNewPassword.Text == this.txtRepeatPassword.Text)
                    {
                        this.txtPassword.Text = this.txtNewPassword.Text;
                    }
                    //Login.DoLogin(this, person);
                    //this.DialogResult = DialogResult.Retry;
                    //this.Close();
                }
                else
                {
                    MessageBox.Show("Die Sicherheitsfragen stimmen nicht überein!");
                    return;
                }
            }
            else if (PasswordChange)
            {
                if (this.txtNewPassword.Text == this.txtRepeatPassword.Text)
                {
                    //this.DialogResult = DialogResult.Yes;
                    NewPassword = txtNewPassword.Text;
                    this.Cancel = false;
                    //this.Close();
                    //Login.DoLogin(this, per);
                }
                else
                {
                    MessageBox.Show("Das neue Passwort stimmt nicht überein! Bitte wiederholen Sie Ihre Eingabe!");
                }
            }
            else
            {
                //this.DialogResult = DialogResult.OK;
                //this.Close();
                person = Person.Get(this.Username);
                if (person == null)
                {
                    MessageBox.Show("Benutzername existiert nicht!");
                }
                this.DoLogin(this, person);
            }
            if (PasswordChange || ForgotPassword)
            {
                person = Person.Get(this.Username);
                DialogResult result = MessageBox.Show("Ihr Passwort wurde erfolgreich geändert! \nWollen Sie nun Ihren neuen Mitarbeiterausweis drucken?", "Close", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Config.PrintUserCard(person, this.NewPassword);
                }
                if (fake)
                {
                    this.Close();
                    this.NewPassword = txtNewPassword.Text;
                    this.DialogResult = DialogResult.Yes;
                    return;
                }
                this.DoLogin(this, person);
                
            }
            try
            {
                if (keyboard != null)
                {
                    if (keyboard.ProcessName == "osk")
                    {
                        keyboard.Kill();
                    }
                }
            }
            catch { }
        }

        public string NewPassword { get; set; }

        public string Username
        {
            get
            {
                return this.txtUser.Text;
            }
        }

        public string Password
        {
            get
            {
                return this.txtPassword.Text;
            }
        }

        public string SecurityQuestion1
        {
            get
            {
                return this.txtSecurityQuestion1.Text;
            }
        }

        public string SecurityQuestion2
        {
            get
            {
                return this.txtSecurityQuestion2.Text;
            }
        }    

        public bool PasswordChange { get; set; }

        public bool ForgotPassword { get; set; }

        public bool Cancel { get; set; }

        public void ShowPasswordChange()
        {
            PasswordChange = true;
            this.lblNewPassword.Visible = true;
            this.lblRepeatPassword.Visible = true;
            this.txtNewPassword.Visible = true;
            this.txtRepeatPassword.Visible = true;
            this.txtRepeatPassword.Visible = true;
            this.Height = 402;
            this.AutoSize = true;
        }

        public void HidePasswordChange()
        {
            PasswordChange = false;
            this.lblNewPassword.Visible = false;
            this.lblRepeatPassword.Visible = false;
            this.txtNewPassword.Visible = false;
            this.txtRepeatPassword.Visible = false;
            this.txtRepeatPassword.Visible = false;            
            LoginForm_Load(null, null);
            this.AutoSize = false;
        }

        public void ShowNewPasswordChange()
        {
            PasswordChange = true;
            this.lblNewPassword.Visible = true;
            this.lblRepeatPassword.Visible = true;
            this.lblSecurityQuestion.Visible = true;
            this.lblSecurityQuestion1.Visible = true;
            this.lblSecurityQuestion2.Visible = true;
            this.txtNewPassword.Visible = true;
            this.txtRepeatPassword.Visible = true;
            this.txtSecurityQuestion1.Visible = true;
            this.txtSecurityQuestion2.Visible = true;
            this.txtRepeatPassword.Visible = true;
            this.Height = 502;
            this.AutoSize = true;
        }

        public void HideNewPasswordChange()
        {
            PasswordChange = false;
            this.lblNewPassword.Visible = false;
            this.lblRepeatPassword.Visible = false;
            this.lblSecurityQuestion.Visible = false;
            this.lblSecurityQuestion1.Visible = false;
            this.lblSecurityQuestion2.Visible = false;
            this.txtNewPassword.Visible = false;
            this.txtRepeatPassword.Visible = false;
            this.txtSecurityQuestion1.Visible = false;
            this.txtSecurityQuestion2.Visible = false;
            this.txtRepeatPassword.Visible = false;
            LoginForm_Load(null, null);
            this.AutoSize = false;
        }

        public void ShowUnlockAccount()
        {
            ShowNewPasswordChange();
            this.txtPassword.Visible = false;
            this.lblPassword.Visible = false;
            PasswordChange = false;
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            this.txtPassword.Visible = true;
            this.lblPassword.Visible = true;
            this.Height = 295;
        }
           
        private void btnInputHelp_Click(object sender, EventArgs e)
        {
            var path64 = @"C:\Windows\WinSxS\amd64_microsoft-windows-osk_31bf3856ad364e35_10.0.16299.15_none_cda8ff5ecb1324ab\osk.exe";
            var path32 = @"C:\windows\system32\osk.exe";
            var path = (Environment.Is64BitOperatingSystem) ? path64 : path32;
            keyboard = Process.Start(path);
            txtUser.Focus();
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Cancel)
            {
                Application.Exit();
            }
            e.Cancel = false;
        }

        private void btnForgotPW_Click(object sender, EventArgs e)
        {
            this.ShowUnlockAccount();
            ForgotPassword = true;
        }

        private void DoLogin(LoginForm loginForm, Person person)
        {
            AccountLog accountLog = person.AccountLogAdd(DateTime.Now, false);
            if (person.AccountState == AccountStateEnum.Gesperrt)
            {
                MessageBox.Show("Account gesperrt, Bitte wenden Sie sich an einen Administrator!");
            }
            else
            {
                if (person.Password != Crypt.HashSHAPassword(loginForm.Password) && person.Password != loginForm.Password)
                {
                    MessageBox.Show("Falsches Passwort!");
                    person.LogonAttempt = person.LogonAttempt.HasValue ? person.LogonAttempt.Value + 1 : 1;
                    if (person.LogonAttempt > 3)
                    {
                        person.AccountState = AccountStateEnum.Gesperrt;
                    }
                    accountLog.Success = false;
                    accountLog.Message = "Login";
                    person.Save();
                    //Application.Restart();
                }
                else
                {
                    accountLog.Success = true;
                    accountLog.Message = "Login";
                    person.LogonAttempt = 0;
                    if (person.AccountState == AccountStateEnum.Neu || person.AccountState == AccountStateEnum.Abgelaufen)
                    {
                        if (person.AccountState == AccountStateEnum.Abgelaufen)
                        {
                            MessageBox.Show("Ihr Account ist abgelaufen, bitte geben Sie ein neues Passwort an!");
                            loginForm.ShowPasswordChange();
                        }
                        if (person.AccountState == AccountStateEnum.Neu)
                        {
                            MessageBox.Show("Neuer Account! \nBitte setzen Sie ein neues Passwort und eine Sicherheitsfrage.");
                            //loginForm.ShowNewPasswordChange();
                        }
                        loginForm.Cancel = true;
                        var newLogin = new LoginForm(true);
                        newLogin.ShowNewPasswordChange();
                        
                        this.NewPassword = newLogin.Password;
                        if (newLogin.ShowDialog() == DialogResult.Yes)
                        {
                            if (person.AccountState == AccountStateEnum.Neu && !loginForm.ForgotPassword)
                            {
                                person.SecurityQuestion1 = newLogin.SecurityQuestion1;
                                person.SecurityQuestion2 = newLogin.SecurityQuestion2;
                                //this.NewPassword = txtPassword.Text;
                            }
                            person.Password = Crypt.HashSHAPassword(newLogin.NewPassword);
                            person.AccountState = AccountStateEnum.Offen;
                            person.PasswordExpiration = DateTime.Now.AddYears(2);
                            person.LogonAttempt = 0;
                        }
                    }
                    person.Save();
                    loginForm.SetInVisible();
                    if (person.PersonType == PersonTypeEnum.Admin)
                    {
                        AdminForm adminForm = new AdminForm(loginForm);
                        adminForm.Show();
                        //Application.Run(new AdminForm());
                    }
                    if (person.PersonType == PersonTypeEnum.Kassier)
                    {
                        KassenForm kassenForm = new KassenForm(loginForm);
                        kassenForm.Show();
                        //Application.Run(new KassenForm());
                    }
                }
            }
        }
    }
}
