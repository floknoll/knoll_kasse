﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class InventoryForm : Form
    {
        Process keyboard = null;

        public InventoryForm()
        {
            InitializeComponent();
            LoadArticle();
        }

        private void btnInputHelp_Click(object sender, EventArgs e)
        {
            var path64 = @"C:\Windows\WinSxS\amd64_microsoft-windows-osk_31bf3856ad364e35_10.0.16299.15_none_cda8ff5ecb1324ab\osk.exe";
            var path32 = @"C:\windows\system32\osk.exe";
            var path = (Environment.Is64BitOperatingSystem) ? path64 : path32;
            keyboard = Process.Start(path);
            txtArticleNo.Focus();
        }

        private void LoadArticle()
        {
            List<Article> articleListe = new List<Article>();
            if (string.IsNullOrEmpty(txtArticleNo.Text) && string.IsNullOrEmpty(txtBarcode.Text) && string.IsNullOrEmpty(txtDescription.Text))
            {
                articleListe = Article.GetList();
            }
            else
            {
                articleListe = Article.GetListLike(txtArticleNo.Text, txtDescription.Text, txtBarcode.Text);
            }
            
            List<ArticleCategory> categorys = ArticleCategory.GetList();

            ListViewItem item = null;
            foreach (Article article in articleListe)
            {
                var category = categorys.Where(w => w.ArticleCategoryId == article.ArticleCategoryId).Select(s => s.Description).FirstOrDefault();
                item = new ListViewItem(article.ArticleNo.ToString());                
                item.SubItems.Add(article.Description);
                item.SubItems.Add(article.Barcode);
                item.SubItems.Add(article.Stock.ToString());
                item.Tag = article;
                this.listViewArticle.Items.Add(item);
                if (this.listViewArticle.Items.Count % 2 == 1) item.BackColor = Color.PeachPuff;
            }
        }

        private void txtArticleNo_TextChanged(object sender, EventArgs e)
        {
            this.listViewArticle.Items.Clear();
            LoadArticle();
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            this.listViewArticle.Items.Clear();
            LoadArticle();
        }

        private void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            this.listViewArticle.Items.Clear();
            LoadArticle();
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (keyboard != null)
                {
                    if (keyboard.ProcessName == "osk")
                    {
                        keyboard.Kill();
                    }
                }
            }
            catch { }

            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void listViewArticle_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewArticle.SelectedItems.Count == 1)
            {
                try
                {
                    KassenForm kassenForm = null;
                    if (Application.OpenForms[1].Text == "Kasse")
                    {
                        kassenForm = Application.OpenForms[1] as KassenForm;
                    }
                    else if (Application.OpenForms[2].Text == "Kasse")
                    {
                        kassenForm = Application.OpenForms[2] as KassenForm;
                    }
                    else if (Application.OpenForms[3].Text == "Kasse")
                    {
                        kassenForm = Application.OpenForms[3] as KassenForm;
                    }
                    else if (Application.OpenForms[4].Text == "Kasse")
                    {
                        kassenForm = Application.OpenForms[4] as KassenForm;
                    }
                    else if (Application.OpenForms[5].Text == "Kasse")
                    {
                        kassenForm = Application.OpenForms[4] as KassenForm;
                    }
                    else if (Application.OpenForms[6].Text == "Kasse")
                    {
                        kassenForm = Application.OpenForms[4] as KassenForm;
                    }
                    kassenForm.AddArticleToList((Article)this.listViewArticle.SelectedItems[0].Tag, 1);
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
