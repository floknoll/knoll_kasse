﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
	class KnollConnection
	{
		#region static section
		private static KnollConnection instance = null;

		public static KnollConnection GetInstance()
		{
			if ( instance == null)
			{
				instance = new KnollConnection();
			}

			return instance;
		}
		#endregion

		//*********************************************************************************************
		#region instance section

		public KnollConnection()
		{
			this.Connection =  new NpgsqlConnection("user id=KnollDB_User;password=knoll#asdf$0815;Server=127.0.0.1;Port=5432;Database=KnollDB;pooling=false;");
		}

		public NpgsqlConnection Connection { get; set; }

		#endregion

	}
}
