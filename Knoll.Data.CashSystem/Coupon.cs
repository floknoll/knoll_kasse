﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Data.Cashsystem
{
    public class Coupon : KnollDataObject
    {

        const string selectCmd = "select coupon_id, value, serial_no, valid, spender, redeemed from knoll_kasse.Coupon";
        const string insertCmd = "insert into knoll_kasse.Coupon (coupon_id, value, serial_no, valid, spender, redeemed) values (:coupon_id, :value, :serial_no, :valid, :spender, :redeemed)";
        const string updateCmd = "update knoll_kasse.Coupon set value = :value, serial_no = :serial_no, valid = :valid, spender = :spender, redeemed = :redeemed where coupon_id = :coupon_id";
        const string deleteCmd = "delete from knoll_kasse.Coupon where coupon_id = :coupon_id";
        //***************************************************************
        #region static methods

        public static List<Coupon> GetList() => GetList<Coupon>($"{selectCmd} order by REDEEMED");

        public static Coupon Get(string serialNo)
        {
            KnollConnection.GetInstance().Connection.Open();
            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = "select * from knoll_kasse.Coupon where serial_no = :serial_no";
            command.Parameters.AddWithValue("serial_no", serialNo);
            NpgsqlDataReader reader = command.ExecuteReader();

            Coupon result = null;

            if (reader.Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int idx = 0; idx < reader.FieldCount; idx++)
                {
                    row.Add(reader.GetName(idx), reader.IsDBNull(idx) ? null : reader.GetValue(idx));
                }
                result = new Coupon(row);
            }
            reader.Close();

            KnollConnection.GetInstance().Connection.Close();
            return result;
        }
        #endregion

        //***************************************************************
        #region constructors
        public Coupon() : base()
        {
            SetValue("coupon_id", -1m);
        }

        public Coupon(KnollDataObject parent) : base(parent)
        {
            SetValue("coupon_id", -1m);
        }

        public Coupon(Dictionary<string, object> row) : base(row)
        {

        }

        public Coupon(KnollDataObject parent, Dictionary<string, object> row) : base(parent, row)
        {

        }
        #endregion

        //***************************************************************
        #region properties
        public decimal CouponId
        {
            get
            {
                return GetValue<decimal>("coupon_id");
            }
        }

        public decimal Value
        {
            get
            {
                return GetValue<decimal>("value");
            }
            set
            {
                SetValue("value", value);
            }
        }

        public string SerialNo
        {
            get
            {
                return GetValue<string>("serial_no");
            }
            set
            {
                SetValue("serial_no", value);
            }
        }

        public bool Valid
        {
            get
            {
                return GetValue<bool>("valid");
            }
            set
            {
                SetValue("valid", value);
            }
        }

        public string ValidText
        {
            get
            {
                switch (Valid)
                {
                    case true:
                        return "Ja";
                    case false:
                        return "Nein";
                    default:
                        break;
                }
                return null;
            }
        }

        public string Spender
        {
            get
            {
                return GetValue<string>("spender");
            }
            set
            {
                SetValue("spender", value);
            }
        }

        public DateTime? Redeemed
        {
            get
            {
                return GetValue<DateTime>("redeemed");
            }
            set
            {
                SetValue("redeemed", value);
            }
        }

        #endregion

        //***************************************************************
        #region public methods
        public bool Save()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;

            if (this.CouponId == -1)
            {
                command.CommandText = "select nextval('knoll_kasse.coupon_seq')";
                SetValue("coupon_id", (decimal)((long)command.ExecuteScalar()));               
                command.CommandText = insertCmd;
            }
            else
            {
                command.CommandText = updateCmd;
            }

            command.Parameters.AddWithValue("coupon_id", this.CouponId);
            command.Parameters.AddWithValue("value", this.Value);
            command.Parameters.AddWithValue("serial_no", this.SerialNo);
            command.Parameters.AddWithValue("valid", this.Valid);
            command.Parameters.AddWithValue("spender", this.Spender);
            command.Parameters.AddWithValue("redeemed", this.Redeemed.HasValue ? (object)this.Redeemed.Value : (object)DBNull.Value);

            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;
        }

        public bool Delete()
        {
            KnollConnection.GetInstance().Connection.Open();

            NpgsqlCommand command = new NpgsqlCommand();
            command.Connection = KnollConnection.GetInstance().Connection;
            command.CommandText = deleteCmd;
            command.Parameters.AddWithValue("coupon_id", this.CouponId);
            int result = command.ExecuteNonQuery();
            KnollConnection.GetInstance().Connection.Close();
            return result == 1;

        }

        #endregion


    }
}
