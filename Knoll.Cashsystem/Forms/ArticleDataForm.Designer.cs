﻿namespace Knoll.Cashsystem
{
    partial class ArticleDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtArticleNo = new System.Windows.Forms.TextBox();
            this.txtStock = new System.Windows.Forms.TextBox();
            this.lblArticleNo = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblStock = new System.Windows.Forms.Label();
            this.btnScanArticle = new System.Windows.Forms.Button();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbArticleCategory = new System.Windows.Forms.ComboBox();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.lblBarcode = new System.Windows.Forms.Label();
            this.lblScanReady = new System.Windows.Forms.Label();
            this.cmbDiscount = new System.Windows.Forms.ComboBox();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(115, 354);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(197, 354);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Abbrechen";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtArticleNo
            // 
            this.txtArticleNo.Enabled = false;
            this.txtArticleNo.Location = new System.Drawing.Point(15, 25);
            this.txtArticleNo.Name = "txtArticleNo";
            this.txtArticleNo.Size = new System.Drawing.Size(81, 20);
            this.txtArticleNo.TabIndex = 15;
            // 
            // txtStock
            // 
            this.txtStock.Location = new System.Drawing.Point(15, 325);
            this.txtStock.Name = "txtStock";
            this.txtStock.Size = new System.Drawing.Size(84, 20);
            this.txtStock.TabIndex = 4;
            // 
            // lblArticleNo
            // 
            this.lblArticleNo.AutoSize = true;
            this.lblArticleNo.Location = new System.Drawing.Point(12, 9);
            this.lblArticleNo.Name = "lblArticleNo";
            this.lblArticleNo.Size = new System.Drawing.Size(53, 13);
            this.lblArticleNo.TabIndex = 4;
            this.lblArticleNo.Text = "Artikel Nr.";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(13, 110);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 5;
            this.lblDescription.Text = "Bezeichung";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(15, 126);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(256, 20);
            this.txtDescription.TabIndex = 1;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(15, 277);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(80, 20);
            this.txtPrice.TabIndex = 3;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(13, 261);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(30, 13);
            this.lblPrice.TabIndex = 8;
            this.lblPrice.Text = "Preis";
            // 
            // lblStock
            // 
            this.lblStock.AutoSize = true;
            this.lblStock.Location = new System.Drawing.Point(12, 309);
            this.lblStock.Name = "lblStock";
            this.lblStock.Size = new System.Drawing.Size(60, 13);
            this.lblStock.TabIndex = 9;
            this.lblStock.Text = "Lagerstand";
            // 
            // btnScanArticle
            // 
            this.btnScanArticle.Location = new System.Drawing.Point(182, 22);
            this.btnScanArticle.Name = "btnScanArticle";
            this.btnScanArticle.Size = new System.Drawing.Size(90, 23);
            this.btnScanArticle.TabIndex = 0;
            this.btnScanArticle.Text = "Artikel scannen";
            this.btnScanArticle.UseVisualStyleBackColor = true;
            this.btnScanArticle.Visible = false;
            this.btnScanArticle.Click += new System.EventHandler(this.btnScanArticle_Click);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(12, 159);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(77, 13);
            this.lblCategory.TabIndex = 11;
            this.lblCategory.Text = "Produktgruppe";
            // 
            // cmbArticleCategory
            // 
            this.cmbArticleCategory.FormattingEnabled = true;
            this.cmbArticleCategory.Location = new System.Drawing.Point(15, 175);
            this.cmbArticleCategory.Name = "cmbArticleCategory";
            this.cmbArticleCategory.Size = new System.Drawing.Size(256, 21);
            this.cmbArticleCategory.TabIndex = 2;
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(15, 75);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(256, 20);
            this.txtBarcode.TabIndex = 0;
            // 
            // lblBarcode
            // 
            this.lblBarcode.AutoSize = true;
            this.lblBarcode.Location = new System.Drawing.Point(12, 59);
            this.lblBarcode.Name = "lblBarcode";
            this.lblBarcode.Size = new System.Drawing.Size(47, 13);
            this.lblBarcode.TabIndex = 15;
            this.lblBarcode.Text = "Barcode";
            // 
            // lblScanReady
            // 
            this.lblScanReady.AutoSize = true;
            this.lblScanReady.Location = new System.Drawing.Point(179, 48);
            this.lblScanReady.Name = "lblScanReady";
            this.lblScanReady.Size = new System.Drawing.Size(0, 13);
            this.lblScanReady.TabIndex = 16;
            // 
            // cmbDiscount
            // 
            this.cmbDiscount.FormattingEnabled = true;
            this.cmbDiscount.Location = new System.Drawing.Point(15, 226);
            this.cmbDiscount.Name = "cmbDiscount";
            this.cmbDiscount.Size = new System.Drawing.Size(256, 21);
            this.cmbDiscount.TabIndex = 17;
            // 
            // lblDiscount
            // 
            this.lblDiscount.AutoSize = true;
            this.lblDiscount.Location = new System.Drawing.Point(12, 210);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(39, 13);
            this.lblDiscount.TabIndex = 18;
            this.lblDiscount.Text = "Rabatt";
            // 
            // ArticleDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(284, 389);
            this.Controls.Add(this.cmbDiscount);
            this.Controls.Add(this.lblDiscount);
            this.Controls.Add(this.lblScanReady);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.lblBarcode);
            this.Controls.Add(this.cmbArticleCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.btnScanArticle);
            this.Controls.Add(this.lblStock);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblArticleNo);
            this.Controls.Add(this.txtStock);
            this.Controls.Add(this.txtArticleNo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ArticleDataForm";
            this.Text = "Artikel bearbeiten";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ArticleDataForm_FormClosing);
            this.Load += new System.EventHandler(this.ArticleDataForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ArticleDataForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtArticleNo;
        private System.Windows.Forms.TextBox txtStock;
        private System.Windows.Forms.Label lblArticleNo;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblStock;
        private System.Windows.Forms.Button btnScanArticle;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbArticleCategory;
        private System.Windows.Forms.TextBox txtBarcode;
        private System.Windows.Forms.Label lblBarcode;
        private System.Windows.Forms.Label lblScanReady;
        private System.Windows.Forms.ComboBox cmbDiscount;
        private System.Windows.Forms.Label lblDiscount;
    }
}