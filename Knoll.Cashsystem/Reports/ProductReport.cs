﻿using Knoll.Data.Cashsystem;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class ProductReport
    {
        public List<Article> Articles { get; set; }
    }
}
