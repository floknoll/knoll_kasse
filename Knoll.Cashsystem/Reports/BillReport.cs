﻿using Knoll.Data.Cashsystem;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knoll.Cashsystem.Reports
{
    public class BillReport
    {
        public List<BillItem> Bill { get; set; }
        public List<InvoiceLine> BillLines { get; set; }

        public void PrintBill(decimal invoiceId, byte[] qrCodeArray)
        {   
            Invoice invoice = Invoice.Get(invoiceId);            
            Bill = new List<BillItem>();
            BillItem bill = new BillItem();
            bill.Invoice = invoice;
            bill.QRCodeArray = qrCodeArray;
            Bill.Add(bill);

            BillLines = InvoiceLine.GetList(invoice);

            LocalReport report = new LocalReport();
            report.ReportEmbeddedResource = "Knoll.Cashsystem.Reports.Bill.rdlc";
            report.DataSources.Add(new ReportDataSource("BillDataSet", Bill));
            report.DataSources.Add(new ReportDataSource("BillLineDataSet", BillLines));
            report.EnableExternalImages = true;

            PrintReport printReport = new PrintReport();
            printReport.Run(report, "Kassabon");
        }
    }

    public class BillItem
    {
        public Invoice Invoice { get; set; }

        public decimal InvoiceNo
        {
            get
            {
                return Invoice.InvoiceNo;
            }
        }

        public decimal NettoA
        {
            get
            {
                return Invoice.NettoA;
            }
        }

        public decimal NettoB
        {
            get
            {
                return Invoice.NettoB;
            }
        }

        public decimal Brutto
        {
            get
            {
                return Invoice.Brutto;
            }
        }

        public decimal BackMoney
        {
            get
            {
                return Invoice.BackMoney;
            }
        }

        public DateTime? InvoiceDate
        {
            get
            {
                return Invoice.InvoiceDate;
            }
        }

        public byte[] QRCodeArray { get; set; }
    }
}
