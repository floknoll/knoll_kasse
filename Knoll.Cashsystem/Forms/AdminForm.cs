﻿using Knoll.Cashsystem.Classes;
using Knoll.Cashsystem.Reports;
using Knoll.Data.Cashsystem;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Knoll.Cashsystem
{
    public partial class AdminForm : Form
    {
        LoginForm loginForm;
        public AdminForm(LoginForm loginForm)
        {
            InitializeComponent();
            this.loginForm = loginForm;
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            LoadArticle();
            LoadUser();
            LoadArticleCategory();
            LoadCoupon();
            LoadDiscount();
            this.reportViewerSales.RefreshReport();
            this.reportViewerProducts.RefreshReport();
            this.reportViewerCoupon.RefreshReport();
            this.reportViewerPriceList.RefreshReport();
            this.reportViewerDiscount.RefreshReport();
        }

        #region AddButtons

        private void btnAddArticle_Click(object sender, EventArgs e)
        {
            ArticleDataForm dataForm = new ArticleDataForm();
            if (dataForm.ShowDialog() == DialogResult.OK)
            {
                this.listViewArticle.Items.Clear();
                LoadArticle();
            }
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            PersonDataForm dataForm = new PersonDataForm();
            if (dataForm.ShowDialog() == DialogResult.OK)
            {
                this.listViewUser.Items.Clear();
                LoadUser();
            }
        }

        private void btnAddArticleCategory_Click(object sender, EventArgs e)
        {
            CategoryDataForm dataForm = new CategoryDataForm();
            if (dataForm.ShowDialog() == DialogResult.OK)
            {
                this.listViewArticleCategory.Items.Clear();
                LoadArticleCategory();
            }
        }

        private void btnAddCoupon_Click(object sender, EventArgs e)
        {
            CouponDataForm dataForm = new CouponDataForm();
            if (dataForm.ShowDialog() == DialogResult.OK)
            {
                this.listViewCoupon.Items.Clear();
                LoadCoupon();
            }
        }

        private void btnAddDiscount_Click(object sender, EventArgs e)
        {
            DiscountDataForm dataForm = new DiscountDataForm();
            if (dataForm.ShowDialog() == DialogResult.OK)
            {
                this.listViewDiscount.Items.Clear();
                LoadDiscount();
            }
        }

        #endregion

        #region DeleteButtons

        private void btnDeleteArticle_Click(object sender, EventArgs e)
        {
            if (this.listViewArticle.SelectedItems.Count == 1)
            {
                ((Article)this.listViewArticle.SelectedItems[0].Tag).Delete();
                this.listViewArticle.Items.Clear();
                LoadArticle();
            }
        }

        private void btnDeleteUser_Click(object sender, EventArgs e)
        {
            if (this.listViewUser.SelectedItems.Count == 1)
            {
                ((Person)this.listViewUser.SelectedItems[0].Tag).Delete();
                this.listViewUser.Items.Clear();
                LoadUser();
            }
        }

        private void btnDeleteArticleCategory_Click(object sender, EventArgs e)
        {
            if (this.listViewArticleCategory.SelectedItems.Count == 1)
            {
                ((ArticleCategory)this.listViewArticleCategory.SelectedItems[0].Tag).Delete();
                this.listViewArticleCategory.Items.Clear();
                LoadArticleCategory();
            }
        }

        private void btnDelCoupon_Click(object sender, EventArgs e)
        {
            if (this.listViewCoupon.SelectedItems.Count == 1)
            {
                ((Coupon)this.listViewCoupon.SelectedItems[0].Tag).Delete();
                this.listViewCoupon.Items.Clear();
                LoadCoupon();
            }
        }

        private void btnDeleteDiscount_Click(object sender, EventArgs e)
        {
            if (this.listViewDiscount.SelectedItems.Count == 1)
            {
                ((Discount)this.listViewDiscount.SelectedItems[0].Tag).Delete();
                this.listViewDiscount.Items.Clear();
                LoadDiscount();
            }
        }

        #endregion

        #region DoubleClicks

        private void listViewArticle_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewArticle.SelectedItems.Count == 1)
            {

                ArticleDataForm dataForm = new ArticleDataForm((Article)this.listViewArticle.SelectedItems[0].Tag);
                if (dataForm.ShowDialog() == DialogResult.OK)
                {
                    this.listViewArticle.Items.Clear();
                    LoadArticle();
                }
            }
        }

        private void listViewUser_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewUser.SelectedItems.Count == 1)
            {

                PersonDataForm dataForm = new PersonDataForm((Person)this.listViewUser.SelectedItems[0].Tag);
                if (dataForm.ShowDialog() == DialogResult.OK)
                {
                    this.listViewUser.Items.Clear();
                    LoadUser();
                }
            }
        }

        private void listViewArticleCategory_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewArticleCategory.SelectedItems.Count == 1)
            {

                CategoryDataForm dataForm = new CategoryDataForm((ArticleCategory)this.listViewArticleCategory.SelectedItems[0].Tag);
                if (dataForm.ShowDialog() == DialogResult.OK)
                {
                    this.listViewArticleCategory.Items.Clear();
                    LoadArticleCategory();
                }
            }
        }

        private void listViewCoupon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewCoupon.SelectedItems.Count == 1)
            {
                CouponDataForm dataForm = new CouponDataForm((Coupon)this.listViewCoupon.SelectedItems[0].Tag);
                if (dataForm.ShowDialog() == DialogResult.OK)
                {
                    this.listViewCoupon.Items.Clear();
                    LoadCoupon();
                }
            }
        }

        private void listViewDiscount_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewDiscount.SelectedItems.Count == 1)
            {
                DiscountDataForm dataForm = new DiscountDataForm((Discount)this.listViewDiscount.SelectedItems[0].Tag);
                if (dataForm.ShowDialog() == DialogResult.OK)
                {
                    this.listViewDiscount.Items.Clear();
                    LoadDiscount();
                }
            }
        }

        #endregion

        #region LoadFunctions

        private void LoadArticle()
        {

            List<Article> articleListe = Article.GetList();
            List<ArticleCategory> categorys = ArticleCategory.GetList();

            ListViewItem item = null;
            foreach (Article article in articleListe)
            {
                var category = categorys.Where(w => w.ArticleCategoryId == article.ArticleCategoryId).Select(s => s.Description).FirstOrDefault();
                item = new ListViewItem(article.ArticleNo.ToString());
                item.SubItems.Add(article.Barcode);
                item.SubItems.Add(article.Description);
                item.SubItems.Add(string.Format("€ {0:n}", article.Price));
                item.SubItems.Add(article.Stock.ToString());
                item.SubItems.Add(category);                               
                item.Tag = article;
                this.listViewArticle.Items.Add(item);
                if (this.listViewArticle.Items.Count % 2 == 1) item.BackColor = Color.PeachPuff;
            }
        }

        private void LoadUser()
        {
            List<Person> personListe = Person.GetList();

            ListViewItem item = null;
            foreach (Person person in personListe)
            {
                item = new ListViewItem(person.PersonalNo.ToString());
                item.SubItems.Add(person.FirstName);
                item.SubItems.Add(person.LastName);
                item.SubItems.Add(person.NickName);
                item.SubItems.Add(string.Format("{0:dd.MM.yyyy}", person.PasswordExpiration));
                item.SubItems.Add(person.PersonTypeText);
                item.SubItems.Add(person.AccountStateText);
                item.SubItems.Add(person.LogonAttempt.ToString());
                item.Tag = person;
                this.listViewUser.Items.Add(item);
                if (this.listViewUser.Items.Count % 2 == 1) item.BackColor = Color.PeachPuff;
            }
        }

        private void LoadArticleCategory()
        {

            List<ArticleCategory> categoryListe = ArticleCategory.GetList();
            List<Discount> discounts = Discount.GetList();

            ListViewItem item = null;
            foreach (ArticleCategory category in categoryListe)
            {
                var discount = discounts.Where(w => w.DiscountId == category.DiscountId).Select(s => s.Description).FirstOrDefault();
                item = new ListViewItem(category.Description);
                item.SubItems.Add(discount);
                item.SubItems.Add(category.Tax);
                item.Tag = category;
                this.listViewArticleCategory.Items.Add(item);
                if (this.listViewArticleCategory.Items.Count % 2 == 1) item.BackColor = Color.PeachPuff;
            }
        }

        private void LoadCoupon()
        {
            List<Coupon> couponListe = Coupon.GetList();

            ListViewItem item = null;
            foreach (Coupon coupon in couponListe)
            {
                item = new ListViewItem(coupon.Spender);
                item.SubItems.Add(coupon.SerialNo);
                item.SubItems.Add(string.Format("€ {0:n}", coupon.Value));
                item.SubItems.Add(coupon.ValidText);
                if (coupon.Redeemed != DateTime.MinValue)
                {
                    item.SubItems.Add(string.Format("{0:dd.MM.yyyy}", coupon.Redeemed));
                }
                item.Tag = coupon;
                this.listViewCoupon.Items.Add(item);
                if (this.listViewCoupon.Items.Count % 2 == 1) item.BackColor = Color.PeachPuff;
            }
        }

        private void LoadDiscount()
        {
            List<Discount> discountListe = Discount.GetList();

            ListViewItem item = null;
            foreach (Discount discount in discountListe)
            {
                if (discount.DiscountId != 1)
                {
                    item = new ListViewItem(discount.Description);
                    item.SubItems.Add(discount.OnePlusOneText);
                    if (discount.Percent != null && discount.Percent != 0)
                    {
                        item.SubItems.Add(string.Format("{0} %", discount.Percent));
                    }
                    item.Tag = discount;
                    this.listViewDiscount.Items.Add(item);
                    if (this.listViewDiscount.Items.Count % 2 == 1) item.BackColor = Color.PeachPuff;
                }
            }
        }

        #endregion

        #region ReportViewer

        private void reportViewerSales_Load(object sender, EventArgs e)
        {            
            BindingList<Invoice> bindingList = new BindingList<Invoice>();
            List<Invoice> invoices = Invoice.GetList();

            foreach (var invoice in invoices)
            {
                bindingList.Add(invoice);
            }
            this.SalesReportBindingSource.DataSource = bindingList;
            this.reportViewerSales.RefreshReport();
        }

        private void reportViewerProducts_Load(object sender, EventArgs e)
        {
            BindingList<Article> bindingList = new BindingList<Article>();
            List<Article> articles = Article.GetList();
            foreach (var article in articles)
            {
                bindingList.Add(article);
            }
            this.ProductReportBindingSource.DataSource = bindingList;
            this.reportViewerProducts.RefreshReport();
        }

        private void reportViewerCoupon_Load(object sender, EventArgs e)
        {
            BindingList<Coupon> bindingList = new BindingList<Coupon>();
            List<Coupon> coupons = Coupon.GetList();
            foreach (var coupon in coupons)
            {
                bindingList.Add(coupon);
            }
            this.CouponReportBindingSource.DataSource = bindingList;
            this.reportViewerCoupon.RefreshReport();
        }

        private void reportViewerPriceList_Load(object sender, EventArgs e)
        {
            BindingList<PriceListItem> bindingList = new BindingList<PriceListItem>();
            List<Article> articles = Article.GetList();
            foreach (var article in articles)
            {
                PriceListItem priceListItem = new PriceListItem();
                priceListItem.Article = article;
                bindingList.Add(priceListItem);
            }
            this.PriceListReportBindingSource.DataSource = bindingList;
            this.reportViewerPriceList.RefreshReport();
        }

        private void reportViewerDiscount_Load(object sender, EventArgs e)
        {
            BindingList<DiscountGrantedItem> bindingList = new BindingList<DiscountGrantedItem>();
            DiscountReport dr = new DiscountReport();
            dr.DiscountsGranted = DiscountGranted.GetList();
            dr.FillDiscounts();
            foreach (var granted in dr.DiscountGrantedItems)
            {
                bindingList.Add(granted);
            }
            this.DiscountReportBindingSource.DataSource = bindingList;
            this.reportViewerDiscount.RefreshReport();
        }

        #endregion

        private void btnPrintUserCard_Click(object sender, EventArgs e)
        {
            if (this.listViewUser.SelectedItems.Count == 1)
            {
                Person person = (Person)this.listViewUser.SelectedItems[0].Tag;
                Config.PrintUserCard(person, person.Password);
            }            
        }

        private void btnPrintCoupon_Click(object sender, EventArgs e)
        {
            if (this.listViewCoupon.SelectedItems.Count == 1)
            {
                Coupon coupon = (Coupon)this.listViewCoupon.SelectedItems[0].Tag;
                Config.PrintCoupon(coupon);
            }

        }

        private void AdminForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
            loginForm.HidePasswordChange();
            loginForm.HideNewPasswordChange();
            loginForm.SetVisible();
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

